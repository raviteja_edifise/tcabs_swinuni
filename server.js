const express = require('express')
const next = require('next')
const compression = require('compression')

const port = parseInt(process.env.PORT, 10) || 3000
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()

app.prepare()
.then(() => {
  const server = express()
  server.use(compression())

  server.get('/about', (req, res) => {
    return app.render(req, res, '/about', req.query)
  })
  server.get('/forgot', (req, res) => {
    return app.render(req, res, '/forgot', req.query)
  })

  // server.get('/posts/:id', (req, res) => {
  //   return app.render(req, res, '/posts', { id: req.params.id })
  // })

  server.get('/admin/employee', (req, res) => {
    return app.render(req, res, '/admin_employee', { id: req.params.id })
  })
  server.get('/admin/student', (req, res) => {
    return app.render(req, res, '/admin_student', { id: req.params.id })
  })
  server.get('/admin/unit', (req, res) => {
    return app.render(req, res, '/admin_unit', { id: req.params.id })
  })
  server.get('/admin/enrol', (req, res) => {
    return app.render(req, res, '/admin_enrol', { id: req.params.id })
  })


  server.get('/convenor/project', (req, res) => {
    return app.render(req, res, '/convenor_project', { id: req.params.id })
  })
  server.get('/convenor/team', (req, res) => {
    return app.render(req, res, '/convenor_team', { id: req.params.id })
  })
  server.get('/convenor/assessment', (req, res) => {
    return app.render(req, res, '/convenor_assessment', { id: req.params.id })
  })

  server.get('/student/dashboard', (req, res) => {
    return app.render(req, res, '/student_dashboard', { id: req.params.id })
  })
  server.get('/student/project', (req, res) => {
    return app.render(req, res, '/student_project', { id: req.params.id })
  })
  server.get('/student/assessment', (req, res) => {
    return app.render(req, res, '/student_assessment', { id: req.params.id })
  })


  server.get('/supervisor/project', (req, res) => {
    return app.render(req, res, '/supervisor_project', { id: req.params.id })
  })
  server.get('/supervisor/assessment', (req, res) => {
    return app.render(req, res, '/supervisor_assessment', { id: req.params.id })
  })



  server.get('*', (req, res) => {
    return handle(req, res)
  })

  server.listen(port, (err) => {
    if (err) throw err
    console.log(`> Ready on http://localhost:${port}`)
  })

});
