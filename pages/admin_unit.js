import Head from 'next/head';
import Link from 'next/link';
import React, { Component } from 'react';
import {Row , Col, Divider } from 'antd';

//styles for components
// import { DatepickerCSS, MenuCSS } from "../styles/styles.js";
import NavBarAdmin from './components/navbar_admin';
import AddUnit from './components/add_unit';
import ListUnits from './components/list_units';
// import EditUnit from './components/edit_unit';

/**
 * this is IndexClass.
 */
export default class extends Component{

  state={
    editData : null
  }

  render(){
    return <div className="header">
      <Head>
        <title>TON ADMIN</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />

      </Head>

      <NavBarAdmin pathname={this.props.url.pathname?this.props.url.pathname:null} />

      <div>

        <AddUnit
          ref={(addunit => { this.addUnit = addunit;})}
          editData={this.state.editData?this.state.editData : null}
         onSave={()=> {

         this.setState({ editData : null});
          this.list_units.refreshList();
        }}/>

        <ListUnits ref={(node)=> {this.list_units = node}}
        onEdit={(item)=>{
          this.setState({ editData : item});
          // if(this.refs.addUnit)
          //   this.refs.addUnit.
        }}
         />
      </div>


    </div>

  }
}
