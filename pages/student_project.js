import Head from 'next/head';
import Link from 'next/link';
import React, { Component } from 'react';
import {Row , Col, Menu, Icon, List, Divider, Select } from 'antd';
const Option = Select.Option;
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

import dbops from './util/dbops';
import Config from './util/config';
//styles for components
// import { DatepickerCSS, MenuCSS } from "../styles/styles.js";
import NavBarStudent from './components/navbar_student';
import ListTeamActivity from './components/list_team_activity';
import ProjectUI from './components/ui_project';

class ProjectName extends React.Component {
  constructor(){
    super();
    this.dbops = new dbops();
    this.state = { name : ''}
  }
  componentDidMount() {
    console.log(this.props.uid);
    if(!this.props.uid)
      return;

    this.dbops.get_data_fire(Config.NETWORK.convenorAddProject , this.props.uid )
    .then( (response) => {
      console.log(response.data());
      this.setState({ name : response.data().projectname});
      //
      // if(this.props.onNameFetched)
      //   this.props.onNameFetched(response.data.user.displayName);

    })
    .catch( (err) => {
      console.log(err);
    });

  }
  render() {
    return (
      <h6>{(this.state.name)}</h6>
    )
  }
}

class SelectProj extends React.Component {

  render() {
    return (<Select
      showSearch
      style={{ width: 200 }}
      placeholder="Select a project"
      optionFilterProp="children"
      onChange={(value)=>{
        alert(value);
      }}
      filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
    >
      {this.props.options.map((item,index) => {
        <Option value={item.projectid}>{item.projectid}</Option>
      })}
    </Select>)
  }
}

/**
 * this is IndexClass.
 */
export default class extends Component{

  state = {
    loading : false,
    user : false,
    pagingCount : 25,
    unitsEnrolled : [],
    chosenProj : null,
    teamMembers : []
  }

  constructor(){
    super();
    this.dbops = new dbops();
  }

  componentDidMount(){

    this.dbops.listen_user_events( (user)=>{
      this.setState({ user : user})
      this.dbops.get_user_type((data)=>{
        //console.log(data);

        this.dbops.log(data.usertype !== data);
        switch(data.usertype){
          case "admin": this.dbops.redirect(Config.url_admin);break;
          case "student": break;
          case "convenor": this.dbops.redirect(Config.url_convenor);break;
          case "supervisor": this.dbops.redirect(Config.url_supervisor);break;
          default :
        }
      });


      this.getUnitsofStudent((res) => {

        let rs = res;
        console.log('ddd',rs);
        this.setState({
          loading: false,
          unitsEnrolled: rs,
        });
      });


    }, (err)=>{
      this.dbops.error('Sign In Error');
    })
  }


  getUnitsofStudent = ( callback) => {
    // debugger;
    console.log(this.dbops.get_current_user())
    this.dbops.queryFS(Config.NETWORK.convenorAddTeamMember,
      [{
        key : "studentid",
        comp : "==",
        val : this.dbops.get_current_user()
      }], this.state.pagingCount, null, null, (querySnapshot)=>{
      let t = [];

      querySnapshot.forEach(function(doc) {
        let ojbm = doc.data();

        ojbm.key = doc.id;
        t.push(ojbm);
      });
      console.log("fetched records", t);
      callback(t);
    }, (err)=>{
      console.log(err);
      this.dbops.error("Error fetching data");
    })

  }


  handleClick = (e) => {
    // console.log('click ', e);
    this.setState({
      chosenProj: null,
    }, ()=>{
      this.setState({ chosenProj : e })
    });
  }

  /**
   * @param {number} a - this is a value.
   * @param {number} b - this is a value.
   * @return {number} result of the sum value.
   */
  render(){
    return <div className="header">
      <Head>
        <title>TON STUDENT</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>

      <NavBarStudent />

      <Row type="flex" justify="center" align="top">
      <Col span={22}>

      {/* LEFT MENU */}
      <Col span={5}>

      {/*
      <h3> Choose Project </h3>
      <Menu
        selectedKeys={[this.state.current]}
        mode="vertical"
      >
        {this.state.unitsEnrolled.map((item,index) => {
          return <Menu.Item key={item.projectid}
            >
            <p onClick={()=>{
              this.handleClick(item)
            }}>
            <Icon type="right" />{item.projectid}
            </p>
          </Menu.Item>
        })}
      </Menu>
      */}

      <List
        size="small"
        header={<div>Choose Project</div>}
        bordered
        dataSource={this.state.unitsEnrolled}
        renderItem={item => (<List.Item onClick={()=>{
          this.handleClick(item)
        }}>
        <List.Item.Meta
          title={<a >{item.teamid}</a>}
        />
        <ProjectName uid={item.projectid}/>
        </List.Item>)}
      />


      {/*
      { this.state.unitsEnrolled.length > 0 &&
        <SelectProj options={this.state.unitsEnrolled} />
      }

      { this.state.unitsEnrolled.length > 0 &&
        <Select
          showSearch
          style={{ width: 200 }}
          placeholder="Select a project"
          optionFilterProp="children"
          onChange={(value)=>{
            alert(value);
          }}
          filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
        >
          {this.state.unitsEnrolled.map((item,index) => {
            <Option value={item.projectid}>{item.projectid}</Option>
          })}
        </Select>
      }

      */}

      </Col>



      {/* RIGHT MENU */}

      <Col span={19}>
      {this.state.chosenProj &&
        <ProjectUI project={this.state.chosenProj} onTeam={(teamMembers)=>{
          console.log(teamMembers);
          this.setState({ teamMembers : teamMembers })
        }} />
      }

      {this.state.chosenProj &&
        <ListTeamActivity projid={this.state.chosenProj} teamMembers={this.state.teamMembers} />
      }

      </Col>
      </Col>
      </Row>

    </div>

  }
}
