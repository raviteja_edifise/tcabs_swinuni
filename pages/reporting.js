
import React, { Component } from 'react';
import {BarChart} from 'react-easy-chart';

export default class extends Component{

  constructor(props){
      super(props);

      let { pathname, query } = props.url;
      this.state = { path : pathname, query : query};

  }

  render(){
      return <div className={'mar25'}>
        Reporting Test
        <p>----------</p>

        <BarChart
          axisLabels={{x: 'My x Axis', y: 'My y Axis'}}
          axes
          height={250}
          width={650}
          colorBars
          xType={'time'}
          data={[
            { x: '1-Jan-15', y: 20 },
            { x: '2-Jan-15', y: 10 },
            { x: '3-Jan-15', y: 33 }
          ]}
        />

      </div>;
  }

}
