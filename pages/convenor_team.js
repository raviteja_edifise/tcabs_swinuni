import Head from 'next/head';
import Link from 'next/link';
import React, { Component } from 'react';
import {Row , Col, Divider } from 'antd';
import moment from 'moment';

//styles for components
// import { DatepickerCSS, MenuCSS } from "../styles/styles.js";
import dbops from './util/dbops';
import NavBarConvenor from './components/navbar_convenor';
import ListProjects from './components/list_projects_simple';
import ListTeams from './components/list_teams';
// import EditUnit from './components/edit_unit';

/**
 * this is IndexClass.
 */
export default class extends Component{

  state={
    choosenData : null
  }

  constructor(){
    super();
    this.dbops = new dbops();
  }

  componentDidMount(){
    this.dbops.listen_user_events( (user)=>{

      this.dbops.get_user_type((data)=>{
        console.log(data);
        this.dbops.log(data.usertype !== data);
        switch(data.usertype){
          case "admin": this.dbops.redirect(Config.url_admin);break;
          case "student": this.dbops.redirect(Config.url_student);break;
          case "convenor": break;
          case "supervisor": this.dbops.redirect(Config.url_supervisor);break;
          default :
        }
      })
    }, (err)=>{
      this.dbops.error('Sign In Error');
    })
  }

  render(){
    return <div className="header">
      <Head>
        <title>TON ADMIN</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />

      </Head>

      <NavBarConvenor pathname={this.props.url.pathname?this.props.url.pathname:null} />

      <Row>
        <Col span={8}>
          <ListProjects onTap={(item)=> {
            // alert(JSON.stringify(item));
            this.setState({ choosenData : item}, ()=>{
              if(this.enrolments)
                this.enrolments.refreshList()
            })

          }}/>
        </Col>
        <Col span={15}>

          <ListTeams
            ref={(node)=>{ this.enrolments = node}}
           chosenUnit={this.state.choosenData? this.state.choosenData : null} />
        </Col>
      </Row>

    </div>

  }
}
