import Head from 'next/head';
import Link from 'next/link';
import React, { Component } from 'react';
import {Row , Col, Menu, Icon } from 'antd';
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

import dbops from './util/dbops';
import Config from './util/config';
//styles for components
// import { DatepickerCSS, MenuCSS } from "../styles/styles.js";
import NavBarStudent from './components/navbar_student';
import UnitAssessments from './components/list_student_assessments';
/**
 * this is IndexClass.
 */
export default class extends Component{

  state = {
    loading : false,
    user : false,
    pagingCount : 25,
    unitsEnrolled : [],
    chosenUnit : null
  }

  constructor(){
    super();
    this.dbops = new dbops();
  }

  componentDidMount(){

    this.dbops.listen_user_events( (user)=>{
      this.setState({ user : user})
      this.dbops.get_user_type((data)=>{
        //console.log(data);

        this.dbops.log(data.usertype !== data);
        switch(data.usertype){
          case "admin": this.dbops.redirect(Config.url_admin);break;
          case "student": break;
          case "convenor": this.dbops.redirect(Config.url_convenor);break;
          case "supervisor": this.dbops.redirect(Config.url_supervisor);break;
          default :
        }
      });


      this.getUnitsofStudent((res) => {

        let rs = res;
        console.log('ddd',rs);
        this.setState({
          loading: false,
          unitsEnrolled: rs,
        });
      });


    }, (err)=>{
      this.dbops.error('Sign In Error');
    })
  }


  getUnitsofStudent = ( callback) => {
    // debugger;
    console.log(this.dbops.get_current_user())
    this.dbops.queryFS(Config.NETWORK.adminAddEnrolmentFire,
      [{
        key : "studentid",
        comp : "==",
        val : this.dbops.get_current_user()
      }], this.state.pagingCount, null, null, (querySnapshot)=>{
      let t = [];

      querySnapshot.forEach(function(doc) {
        let ojbm = doc.data();

        ojbm.key = doc.id;
        t.push(ojbm);
      });
      console.log("fetched records", t);
      callback(t);
    }, (err)=>{
      console.log(err);
      this.dbops.error("Error fetching data");
    })

  }


  handleClick = (e) => {
    // console.log('click ', e);
    this.setState({
      chosenUnit: null,
    }, ()=>{
      this.setState({ chosenUnit : e.key })
    });
  }

  /**
   * @param {number} a - this is a value.
   * @param {number} b - this is a value.
   * @return {number} result of the sum value.
   */
  render(){
    return <div className="header">
      <Head>
        <title>TON STUDENT</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>

      <NavBarStudent />

      <Row type="flex" justify="center" align="top">
      <Col span={22}>

      {/* LEFT MENU */}
      <Col span={5}>

      <h3> Choose Unit </h3>
      <Menu
        onClick={this.handleClick}
        selectedKeys={[this.state.current]}
        mode="vertical"
      >
        {this.state.unitsEnrolled.map((item,index) => {
          return <Menu.Item key={item.unitid}>
            <Icon type="right" />{item.unitid}
          </Menu.Item>
        })}
      </Menu>

      </Col>

      {/* RIGHT MENU */}
      <Col span={19}>

      {this.state.chosenUnit &&
        <UnitAssessments unitid={this.state.chosenUnit} />
      }

      </Col>
      </Col>
      </Row>

    </div>

  }
}
