import Head from 'next/head';
import Link from 'next/link';
import React, { Component } from 'react';
import {Row , Col, Divider } from 'antd';
import moment from 'moment';

import dbops from './util/dbops';
//styles for components
// import { DatepickerCSS, MenuCSS } from "../styles/styles.js";
import NavBarAdmin from './components/navbar_admin';
import ListUnits from './components/list_units_simple';
import EnrolledStudents from './components/list_enrolled_students';
// import EditUnit from './components/edit_unit';

/**
 * this is IndexClass.
 */
export default class extends Component{

  state={
    choosenData : null
  }

  constructor(){
    super()
    this.dbops = new dbops();
  }

  componentDidMount(){
    this.dbops.listen_user_events( (user)=>{

      this.dbops.get_user_type((data)=>{
        console.log(data);
        this.dbops.log(data.usertype !== data);
        switch(data.usertype){
          case "admin": break;
          case "student": this.dbops.redirect(Config.url_student);break;
          case "convenor": this.dbops.redirect(Config.url_convenor);break;
          case "supervisor": this.dbops.redirect(Config.url_supervisor);break;
          default :
        }
      })
    }, (err)=>{
      this.dbops.error('Sign In Error');
    })
  }

  render(){
    return <div className="header">
      <Head>
        <title>TON ADMIN</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />

      </Head>

      <NavBarAdmin pathname={this.props.url.pathname?this.props.url.pathname:null} />

      <Row>
        <Col span={8}>
          <ListUnits onTap={(item)=> {
            // alert(JSON.stringify(item));
            this.setState({ choosenData : item}, ()=>{
              if(this.enrolments)
                this.enrolments.refreshList()
            })

          }}/>
        </Col>
        <Col span={15}>
          <EnrolledStudents
            ref={(node)=>{ this.enrolments = node}}
           chosenUnit={this.state.choosenData? this.state.choosenData : null} />
        </Col>
      </Row>

    </div>

  }
}
