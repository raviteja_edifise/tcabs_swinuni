import Head from 'next/head';
import Link from 'next/link';
import React, { Component } from 'react';
import {Row , Col } from 'antd';

import dbops from './util/dbops';
import Config from './util/config';
//styles for components
// import { DatepickerCSS, MenuCSS } from "../styles/styles.js";
import NavBarSupervisor from './components/navbar_supervisor';

/**
 * this is IndexClass.
 */
export default class extends Component{

  state = {
    loading : false
  }

  constructor(){
    super();
    this.dbops = new dbops();
  }

  componentDidMount(){

    this.dbops.listen_user_events( (user)=>{

      this.dbops.get_user_type((data)=>{
        console.log(data);
        this.dbops.log(data.usertype !== data);
        switch(data.usertype){
          case "admin": this.dbops.redirect(Config.url_admin);break;
          case "student": this.dbops.redirect(Config.url_student);break;
          case "convenor": this.dbops.redirect(Config.url_convenor);break;
          case "supervisor": break;
          default :
        }
      })
    }, (err)=>{
      this.dbops.error('Sign In Error');
    })
  }

  /**
   * @param {number} a - this is a value.
   * @param {number} b - this is a value.
   * @return {number} result of the sum value.
   */
  render(){
    return <div className="header">
      <Head>
        <title>TON STUDENT</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>

      <NavBarSupervisor />

    </div>

  }
}
