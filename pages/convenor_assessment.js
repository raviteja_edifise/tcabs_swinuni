import Head from 'next/head';
import Link from 'next/link';
import React, { Component } from 'react';
import {Row , Col, Divider, Button} from 'antd';

//styles for components
// import { DatepickerCSS, MenuCSS } from "../styles/styles.js";
import dbops from './util/dbops';
import NavBarConvenor from './components/navbar_convenor';
import AddAssessment from './components/add_assessment';
import ListAssessments from './components/list_assessments';
// import EditUnit from './components/edit_unit';

/**
 * this is IndexClass.
 */
export default class extends Component{

  state={
    editData : null,
    data : []
  }

  constructor(){
    super();
    this.dbops = new dbops();
  }

  componentDidMount(){
    this.dbops.listen_user_events( (user)=>{

      this.dbops.get_user_type((data)=>{
        console.log(data);
        this.dbops.log(data.usertype !== data);
        switch(data.usertype){
          case "admin": this.dbops.redirect(Config.url_admin);break;
          case "student": this.dbops.redirect(Config.url_student);break;
          case "convenor": break;
          case "supervisor": this.dbops.redirect(Config.url_supervisor);break;
          default :
        }
      })
    }, (err)=>{
      this.dbops.error('Sign In Error');
    })
  }

  render(){
    return <div className="header">
      <Head>
        <title>TON ADMIN</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />

      </Head>

      <NavBarConvenor pathname={this.props.url.pathname?this.props.url.pathname:null} />

      <Row type="flex" justify="end" align="top">
      <Col span={5}>
        <Button icon={'plus'} size="large" type="primary" className={' bgGreen bGreen'}
          onClick={()=>{
            this.setState({ visible : true});
          }}
        >Add Assessment</Button>
      </Col>
      </Row>

        <AddAssessment
        editData={this.state.editData}
        visible={this.state.visible}
        onSave={(newData)=>{
          //this.dbops.success('Updated Successfully');

          if(this.list_projects)
            this.list_projects.refreshList();
          this.setState({visible : false, editData: false});
        }}
        onCancel={()=>{
          this.setState({ visible : false, editData : false});
        }}
        />



        <ListAssessments ref={(node)=> {this.list_projects = node}}
        onEdit={(item)=>{
          this.setState({ editData : item});
          // if(this.refs.addUnit)
          //   this.refs.addUnit.
        }}
         />



    </div>

  }
}
