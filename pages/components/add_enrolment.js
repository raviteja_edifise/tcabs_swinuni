import React from 'react';


import { Form, Input, Tooltip, Icon, Cascader, Select, Row, Col, Divider,
  Checkbox, Button, AutoComplete,message, Modal, DatePicker, Radio, Badge, Spin } from 'antd';
import moment from 'moment';
import is from 'is_js';

// import { Menu, Icon, Badge, Form, Button, message, Modal } from 'antd';
const FormItem = Form.Item;
const Option = Select.Option;
const AutoCompleteOption = AutoComplete.Option;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

import Config from '../util/config';
import CustomError from '../util/errors/errors';
import { validateEmail, PhoneNumberValidate, formatPhoneNumber, mergeObjs, removeKey } from '../util/util';
import dbops from '../util/dbops';

class AddUnit extends React.Component{
  state = {
    data: null,
    loading : false,
    dataSource: [],
    lastSearch: '',
    searchId : null
  }
  constructor(props){
      super(props);
      this.dbops = new dbops();
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const { validateFieldsAndScroll, validateFields, resetFields } = this.props.form;
    validateFields(['searchId'], { force : true },(err, values) => {
      if (!err) {
        // console.log('Received values of form: ', values);

        this.setState({ loading: true })
        var editObj = {};
        editObj.unitid = this.props.chosenUnit.unitid;
        editObj.studentid = values.searchId;

        //save_data_fire_push = (collection,value, callback, error)
        // this.dbops.save_data_push(Config.NETWORK.adminAddEnrolment+'/', editObj,(resp)=>{
        //   this.dbops.success('Enroled successfully');
        //   this.setState({ loading : false });
        //   if(this.props.onSave)
        //       this.props.onSave();
        //   resetFields();
        //   // this.dbops.redirect(Config.url_admin);
        // }, (err) => {
        //   // debugger;
        //   console.log(err);
        //   this.dbops.error('Error in operation');
        //   this.setState({ loading : false });
        // })

        //  Firestore
        this.dbops.save_data_fire_push(Config.NETWORK.adminAddEnrolment, editObj,(resp)=>{
          this.dbops.success('Enroled successfully');
          this.setState({ loading : false });
          if(this.props.onSave)
              this.props.onSave();
          resetFields();
          // this.dbops.redirect(Config.url_admin);
        }, (err) => {
          // debugger;
          console.log(err);
          this.dbops.error('Error in operation');
          this.setState({ loading : false });
        })


      }
    });
  }

  handleSearch = (value) => {
    if(value.length <= 4)
    {
      this.setState({
        dataSource : []
      })
    }

    if(value.length > 4 && value != this.state.lastSearch){
      this.setState({ lastSearch : value, dataSource : []});

      //autocomplete_fire = (collection, queryKey, queryString, limit, callback, error)
      this.dbops.autocomplete_fire('students', 'uid', value, 2, (querySnapshot)=>{
        let t = [];
        querySnapshot.forEach(function(doc) {
          // console.log(doc.id, " => ", doc.data());
          t.push(doc.id);
        });
        this.setState({ dataSource : t});
        // console.log(child);
      }, (err)=>{
        // console.log(err);
        this.dbops.error("Error fetching data");
      })

      // this.dbops.autocomplete('/students/', value, (child)=>{
      //   let t = this.state.dataSource;
      //   t.push(child.id);
      //   this.setState({ dataSource : t});
      //   // console.log(child);
      // }, (err)=>{
      //   this.dbops.error("Error fetching data");
      // })


    }
  }

  onSelect = (value) => {
    let { setFieldsValue } = this.props.form;
    setFieldsValue({ searchId : value})
    // console.log('onSelect', value);
    // this.setState({ searchId : value})
  }

  render(){
    const { getFieldDecorator } = this.props.form;
    const { dataSource } = this.state

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 16,
          offset: 8,
        },
      },
    };

      return (
        <Row type="flex" justify="center" align="top">

        <Col span={24}>
          <Spin spinning={this.state.loading} >
          {/* <style jsx global>{MenuCSS}</style> */}

          <Form layout="inline" className={' marLR25'} onSubmit={this.handleSubmit}>
          <Divider />
          <FormItem
            {...formItemLayout}
            label={(
              <span>
                 Student ID&nbsp;
              </span>
            )}
          >
            {getFieldDecorator('searchId', {
              initialValue : this.state.searchId ? this.state.searchId : '',
              rules: [{ required: true, message: 'cant be empty!', whitespace: false },
              { validator : (rule, value, callback)=> {
                //const { getFieldValue } = this.props.form;
                let unitid = value;

                if( (''+unitid).length >3 && (''+unitid).length <= 11 )
                {
                  if(!isNaN(parseInt(unitid)) )
                    callback();
                  else
                    callback('Cannot have special characters');
                }
                callback(' Ex : 1010220332 && no spaces && length 4 to 8 letters');
              }}
            ],
            })(
              <AutoComplete
                dataSource={dataSource}
                style={{ width: 200 }}
                onSelect={this.onSelect}
                onSearch={this.handleSearch}
                placeholder="Search student"
              />
            )}
          </FormItem>

          <FormItem>
            <Button
              type="submit"
              htmlType="submit"
            >
              Enrol to this Unit
            </Button>
          </FormItem>

          </Form>

        </Spin>

        <br/>
        <br/>
          </Col>
        </Row>
      );
  }
}

const AddUnitComp = Form.create()(AddUnit);
export default AddUnitComp;
