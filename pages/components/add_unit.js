import React from 'react';


import { Form, Input, Tooltip, Icon, Cascader, Select, Row, Col, Divider,
  Checkbox, Button, AutoComplete,message, Modal, DatePicker, Radio, Badge, Spin } from 'antd';
import moment from 'moment';
import is from 'is_js';

// import { Menu, Icon, Badge, Form, Button, message, Modal } from 'antd';
const FormItem = Form.Item;
const Option = Select.Option;
const AutoCompleteOption = AutoComplete.Option;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

import Config from '../util/config';
import CustomError from '../util/errors/errors';
import { validateEmail, PhoneNumberValidate, formatPhoneNumber, mergeObjs, removeKey } from '../util/util';
import dbops from '../util/dbops';

class AddUnit extends React.Component{
  state = {
    data: null,
    loading : false,
    autoCompleteResult: [],
  }
  constructor(props){
      super(props);
      this.dbops = new dbops();
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const { validateFieldsAndScroll, validateFields, resetFields } = this.props.form;
    validateFields(['unitid', 'unitname'], { force : true },(err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);

        this.setState({ loading: true })
        var editObj = {};
        editObj.unitid = values.unitid;
        editObj.unitname = values.unitname;
        editObj.status = "active";
        //this.dbops.addUser

        this.dbops.save_data_fire(Config.NETWORK.adminAddUnitFire, values.unitid, editObj,(resp)=>{
          //console.log(resp);
          this.dbops.success('Unit saved successfully');
          this.setState({ loading : false });
          if(this.props.onSave)
              this.props.onSave();
          resetFields();
          // this.dbops.redirect(Config.url_admin);
        }, (err) => {
          // debugger;
          // console.log(err);
          this.dbops.error('Error in operation');
          this.setState({ loading : false });
        })
        // this.dbops.save_data(Config.NETWORK.adminAddUnit+'/'+values.unitid, editObj,(resp)=>{
        //   console.log(resp);
        //   this.dbops.success('Unit saved successfully');
        //   this.setState({ loading : false });
        //   if(this.props.onSave)
        //       this.props.onSave();
        //   resetFields();
        //   // this.dbops.redirect(Config.url_admin);
        // }, (err) => {
        //   // debugger;
        //   console.log(err);
        //   this.dbops.error('Error in operation');
        //   this.setState({ loading : false });
        // })


      }
    });
  }

  render(){
    const { getFieldDecorator } = this.props.form;
    const { autoCompleteResult } = this.state;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 16,
          offset: 8,
        },
      },
    };

      return (
        <Row type="flex" justify="center" align="top">

        <Col span={16}>
          <Spin spinning={this.state.loading} >
          {/* <style jsx global>{MenuCSS}</style> */}

          <Form layout="inline" className={' marLR25'} onSubmit={this.handleSubmit}>

          <FormItem
            {...formItemLayout}
            label={(
              <span>
                Unit ID&nbsp;
              </span>
            )}
          >
            {getFieldDecorator('unitid', {
              initialValue : this.props.editData ? this.props.editData.unitid : '',
              rules: [{ required: true, message: 'cant be empty!', whitespace: false },
              { validator : (rule, value, callback)=> {
                //const { getFieldValue } = this.props.form;
                let unitid = value;

                if( (''+unitid).length >3 && (''+unitid).length < 9 )
                {
                  if(is.alphaNumeric(unitid) )
                    callback();
                  else
                    callback('Cannot have special characters');
                }
                callback(' Ex : INF80013 && no spaces && length 4 to 8 letters');
              }}
            ],
            })(
              <Input />
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label={(
              <span>
                Unit Name&nbsp;
              </span>
            )}
          >
            {getFieldDecorator('unitname', {
              initialValue : this.props.editData ? this.props.editData.unitname : '',
              rules: [{ required: true, message: 'cant be empty!', whitespace: false },
              { validator : (rule, value, callback)=> {
                //const { getFieldValue } = this.props.form;
                let unitname = value;

                if( (''+unitname).length > 5 )
                {
                  if(is.string(unitname))
                    callback();
                  else
                    callback('Cannot have numbers or special characters');
                }
                callback('Enter more characters');
              }}
            ],
            })(
              <Input />
            )}
          </FormItem>

          <FormItem>
            <Button
              type="submit"
              htmlType="submit"
            >
              Save
            </Button>
          </FormItem>

          </Form>

        </Spin>

        <br/>
        <br/>
          </Col>
        </Row>
      );
  }
}

const AddUnitComp = Form.create()(AddUnit);
export default AddUnitComp;
