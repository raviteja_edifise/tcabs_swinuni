import React from 'react';


import { Form, Input, Tooltip, Icon, Cascader, Select, Row, Col, Divider,List,
  Checkbox, Button, AutoComplete,message, Modal, DatePicker, Radio, Badge, Spin } from 'antd';
import moment from 'moment';
import is from 'is_js';

// import { Menu, Icon, Badge, Form, Button, message, Modal } from 'antd';
const FormItem = Form.Item;
const Option = Select.Option;
const AutoCompleteOption = AutoComplete.Option;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

import Config from '../util/config';
import CustomError from '../util/errors/errors';
import { validateEmail, PhoneNumberValidate, formatPhoneNumber, mergeObjs, removeKey } from '../util/util';
import dbops from '../util/dbops';

class AddUnit extends React.Component{
  state = {
    loading: false,
    loadingMore: false,
    showLoadingMore: false,
    data: [],
    dataSource: [],
    lastSearch: '',
    searchId : null,
    dataSource2: [],
    lastSearch2: '',
    searchId2 : null,
    pagingCount : 20
  }
  constructor(props){
      super(props);
      this.dbops = new dbops();
      // this.loadTeam = this.loadTeam.bind(this);
  }

  saveSupervisor = (e) => {
    e.preventDefault();
    const { validateFieldsAndScroll, validateFields, resetFields } = this.props.form;
    validateFields(['searchId2'], { force : true },(err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);

        this.setState({ loading: true })
        var editObj = {};
        editObj.projectid = this.props.chosenTeam.projectid;
        editObj.teamid = this.props.chosenTeam.teamid;
        editObj.teamKey = this.props.chosenTeam.key;
        editObj.supervisorid = values.searchId2;

        this.checkIfSupervisorExists(editObj.teamid, editObj.projectid, editObj.supervisorid, (r)=>{
          if(r.length <= 0){
            //  Firestore
            this.dbops.save_data_fire_push(Config.NETWORK.convenorSupervisor, editObj,(resp)=>{
              this.dbops.success('Supervisor added successfully');
              this.setState({ loading : false });
              if(this.props.onCancel)
                this.props.onCancel();
              // if(this.props.onSave)
              //     this.props.onSave();
              // resetFields();
              // this.dbops.redirect(Config.url_admin);
            }, (err) => {
              // debugger;
              console.log(err);
              this.dbops.error('Error in operation');
              this.setState({ loading : false });
            })
          }else {
            this.dbops.error('Supervisor already exists');
            this.setState({ loading : false });

          }
        });


      }
    });
  }

  checkIfSupervisorExists = (teamid, projid, supervisorid, callback) => {
    if(teamid && projid && supervisorid ){
      this.dbops.queryFS(Config.NETWORK.convenorSupervisor,
        [{
          key : "teamid",
          comp : "==",
          val : teamid
        },{
          key : "projectid",
          comp : "==",
          val : projid
        }], 2, null, null, (querySnapshot)=>{
        let t = [];
        querySnapshot.forEach(function(doc) {
          t.push(doc.id);
        });
        console.log("fetched records", t);
        callback(t);
      }, (err)=>{
        // console.log(err);
        this.dbops.error("Error fetching data");
      })
    }
  }

  saveTeamMember = (e) => {
    e.preventDefault();
    const { validateFieldsAndScroll, validateFields, resetFields } = this.props.form;
    validateFields(['searchId'], { force : true },(err, values) => {
      if (!err) {
        // console.log('Received values of form: ', values);

        this.setState({ loading: true })
        var editObj = {};
        editObj.projectid = this.props.chosenTeam.projectid;
        editObj.teamid = this.props.chosenTeam.teamid;
        editObj.teamKey = this.props.chosenTeam.key;
        editObj.studentid = values.searchId;

        this.checkIfmemberExists(editObj.teamid, editObj.projectid, editObj.studentid, (r)=>{
          if(r.length <= 0){
            //  Firestore
            this.dbops.save_data_fire_push(Config.NETWORK.convenorAddTeamMember, editObj,(resp)=>{
              this.dbops.success('Team Member added successfully');
              this.setState({ loading : false });
              if(this.props.onCancel)
                this.props.onCancel();
              // if(this.props.onSave)
              //     this.props.onSave();
              // resetFields();
              // this.dbops.redirect(Config.url_admin);
            }, (err) => {
              // debugger;
              console.log(err);
              this.dbops.error('Error in operation');
              this.setState({ loading : false });
            })
          }else {
            this.dbops.error('Team Member already registered to this or other team');
            this.setState({ loading : false });

          }
        });


      }
    });
  }

  checkIfmemberExists = (teamid, projid, stuid, callback) => {
    if(teamid && projid && stuid ){
      this.dbops.queryFS(Config.NETWORK.convenorAddTeamMember,
        [{
          key : "projectid",
          comp : "==",
          val : projid
        },{
          key : "studentid",
          comp : "==",
          val : stuid
        }], 2, null, null, (querySnapshot)=>{
        let t = [];
        querySnapshot.forEach(function(doc) {
          t.push(doc.id);
        });
        console.log("fetched records", t);
        callback(t);
      }, (err)=>{
        // console.log(err);
        this.dbops.error("Error fetching data");
      })
    }
  }

  handleSearch = (value, callback) => {
    if(value.length <= 4)
    {
      this.setState({
        dataSource : []
      })
    }

    if(value.length > 4 && value != this.state.lastSearch){
      this.setState({ lastSearch : value, dataSource : []});
      //autocomplete_fire = (collection, queryKey, queryString, limit, callback, error)
      this.dbops.autocomplete_fire('students', 'uid', value, 2, (querySnapshot)=>{
        let t = [];
        querySnapshot.forEach(function(doc) {
          // console.log(doc.id, " => ", doc.data());
          t.push(doc.id);
        });
        this.setState({ dataSource : t});
      }, (err)=>{this.dbops.error("Error fetching data");})
    }
  }

  supervisorSearch = (value, callback) => {
    if(value.length <= 4)
    {
      this.setState({
        dataSource : []
      })
    }

    if(value.length > 4 && value != this.state.lastSearch2){
      this.setState({ lastSearch2 : value, dataSource2 : []});
      //autocomplete_fire = (collection, queryKey, queryString, limit, callback, error)
      this.dbops.autocomplete_fire('supervisor', 'uid', value, 2, (querySnapshot)=>{
        let t = [];
        querySnapshot.forEach(function(doc) {
          console.log(doc.id, " => ", doc.data());
          t.push(doc.id);
        });
        this.setState({ dataSource2 : t});
      }, (err)=>{this.dbops.error("Error fetching data");})
    }
  }

  onSupervisorSelect = (value) => {
    let { setFieldsValue } = this.props.form;
    setFieldsValue({ searchId2 : value})
    // console.log('onSelect', value);
    // this.setState({ searchId : value})
  }

  onSelect = (value) => {
    let { setFieldsValue } = this.props.form;
    setFieldsValue({ searchId : value})
    // console.log('onSelect', value);
    // this.setState({ searchId : value})
  }

  componentDidMount() {
  }

  componentWillReceiveProps(nextProps){
}


  removeMember = (item) => {
    console.log(item);
    //remove_data_fire = (collection, doc, callback, error) => {
    this.dbops.remove_data_fire(Config.NETWORK.convenorAddTeamMember, item.key,
      (r)=>{
        this.dbops.success("Member removed successfully");

          if(this.props.onCancel)
            this.props.onCancel();
      },
      (e)=>{
        this.dbops.success("Error removing Member");
      }
    );
  }


  removeSupervisor = (item) => {
    console.log(item);
    //remove_data_fire = (collection, doc, callback, error) => {
    this.dbops.remove_data_fire(Config.NETWORK.convenorSupervisor, item.key,
      (r)=>{
        this.dbops.success("Supervisor removed successfully");

          if(this.props.onCancel)
            this.props.onCancel();
      },
      (e)=>{
        this.dbops.success("Error removing supervisor");
      }
    );
  }

  render(){
    const { getFieldDecorator } = this.props.form;
    const { dataSource, dataSource2 } = this.state;

    const { loading, loadingMore, showLoadingMore, data } = this.state;

    const loadMore = showLoadingMore ? (
      <div style={{ textAlign: 'center', marginTop: 12, height: 50, lineHeight: '50px' }}>
        {loadingMore && <Spin />}
        {!loadingMore && <Button className={' '} onClick={this.onLoadMore}>loading more</Button>}
      </div>
    ) : null;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 16,
          offset: 8,
        },
      },
    };

      return (
        <Modal
          title="Team Manage"
          wrapClassName="vertical-center-modal"
          okText=" Save "
          width={'80%'}
          visible={(!!this.props.chosenProject && !!this.props.chosenTeam) || this.props.visible}
          onOk={this.onSave}
          onCancel={() => {
            if(this.props.onCancel)
              this.props.onCancel();
          }}
        >
        <Row type="flex" justify="center" align="top">

        <Col span={24}>
          <Spin spinning={this.state.loading} >
            {/*
          {JSON.stringify(this.props.chosenTeam)}
          {JSON.stringify(this.props.chosenProject)}
        <style jsx global>{MenuCSS}</style> */}

          <Form layout="inline" className={' marLR25'} onSubmit={this.saveSupervisor}>
          <Divider />
          <FormItem
            {...formItemLayout}
            label={(
              <span>
                 Supervisor ID&nbsp;
              </span>
            )}
          >
            {getFieldDecorator('searchId2', {
              initialValue : this.state.searchId2 ? this.state.searchId2 : '',
              rules: [{ required: true, message: 'cant be empty!', whitespace: false },
              { validator : (rule, value, callback)=> {
                //const { getFieldValue } = this.props.form;
                let unitid = value;

                if( (''+unitid).length >3 && (''+unitid).length <= 11 )
                {
                  if(!isNaN(parseInt(unitid)) )
                    callback();
                  else
                    callback('Cannot have special characters');
                }
                callback(' Ex : 1010220332 && no spaces && length 4 to 8 letters');
              }}
            ],
            })(
              <AutoComplete
                dataSource={dataSource2}
                style={{ width: 200 }}
                onSelect={this.onSupervisorSelect}
                onSearch={this.supervisorSearch}
                placeholder="Search supervisor"
              />
            )}
          </FormItem>

          <FormItem>
            <Button
              type="submit"
              htmlType="submit"
            >
              Add Supervisor
            </Button>
          </FormItem>

          </Form>

          <List
            className="demo-loadmore-list"
            loading={loading}
            itemLayout="horizontal"
            loadMore={loadMore}
            dataSource={this.props.chosenTeamSupervisor ? this.props.chosenTeamSupervisor : []}
            renderItem={item => (
              <List.Item actions={[
                <Button type="danger" onClick={
                  ()=>{
                    this.removeSupervisor(item);
                  }} >{'Remove'}</Button>]}>
                <List.Item.Meta
                  title={<a href="#">{item.supervisorid}</a>}
                />
              </List.Item>
            )}
          />



            <Form layout="inline" className={' marLR25'} onSubmit={this.saveTeamMember}>
            <Divider />
            <FormItem
              {...formItemLayout}
              label={(
                <span>
                   Student ID&nbsp;
                </span>
              )}
            >
              {getFieldDecorator('searchId', {
                initialValue : this.state.searchId ? this.state.searchId : '',
                rules: [{ required: true, message: 'cant be empty!', whitespace: false },
                { validator : (rule, value, callback)=> {
                  //const { getFieldValue } = this.props.form;
                  let unitid = value;

                  if( (''+unitid).length >3 && (''+unitid).length <= 11 )
                  {
                    if(!isNaN(parseInt(unitid)) )
                      callback();
                    else
                      callback('Cannot have special characters');
                  }
                  callback(' Ex : 1010220332 && no spaces && length 4 to 8 letters');
                }}
              ],
              })(
                <AutoComplete
                  dataSource={dataSource}
                  style={{ width: 200 }}
                  onSelect={this.onSelect}
                  onSearch={this.handleSearch}
                  placeholder="Search student"
                />
              )}
            </FormItem>

            <FormItem>
              <Button
                type="submit"
                htmlType="submit"
              >
                Add to team
              </Button>
            </FormItem>

            </Form>

        </Spin>


        <List
          className="demo-loadmore-list"
          loading={loading}
          itemLayout="horizontal"
          loadMore={loadMore}
          dataSource={this.props.chosenTeamMembers ? this.props.chosenTeamMembers : []}
          renderItem={item => (
            <List.Item actions={[
              <Button type="danger" onClick={
                ()=>{
                  this.removeMember(item);
                }} >{'Remove'}</Button>]}>
              <List.Item.Meta
                title={<a href="#">{item.studentid}</a>}
              />
            </List.Item>
          )}
        />


          </Col>
        </Row>
        </Modal>
      );
  }
}

const AddUnitComp = Form.create()(AddUnit);
export default AddUnitComp;
