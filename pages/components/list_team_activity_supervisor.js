import React from 'react';
import { List, Avatar, Button, Spin, Input,DatePicker, Divider, Tag, Select, Tabs, Modal, Badge, message, Col, Row, Collapse, Card, Icon,  Upload } from 'antd';
const { Meta } = Card;
const Panel = Collapse.Panel;
const confirm = Modal.confirm
const ButtonGroup = Button.Group;
const TabPane = Tabs.TabPane;
const Option = Select.Option;
const { TextArea } = Input;
const { RangePicker } = DatePicker;
// import reqwest from 'reqwest';
import {Timeline, TimelineEvent} from 'react-event-timeline';
import TimelineCalendar from 'react-calendar-timeline/lib';
import Head from 'next/head';
// make sure you include the timeline stylesheet or the timeline will not be styled
// import 'react-calendar-timeline/lib/Timeline.css';
import moment from 'moment';
import TimelineStyles from 'react-calendar-timeline/lib/Timeline.css';

console.log(TimelineStyles);
import dbops from '../util/dbops';
import Config from '../util/config';
import { mergeObjs, getIndexOfAttrInObj } from '../util/util';
// import UploadComp from './upload';
import AssessmentItem from './item_assessment';

import ProjectUI from './ui_project';
import UploadComp from './upload.js';


let groups = [{ id: 101028320, title: 'group 1' }, { id: 2, title: 'group 2' }]

let items = [
  {
    id: 1,
    group: 1,
    title: 'item 1',
    start_time: moment(),
    end_time: moment().add(1, 'hour')
  },
  {
    id: 2,
    group: 2,
    title: 'item 2',
    start_time: moment().add(-0.5, 'hour'),
    end_time: moment().add(0.5, 'hour')
  },
  {
    id: 3,
    group: 1,
    title: 'item 3',
    start_time: moment().add(2, 'hour'),
    end_time: moment().add(3, 'hour')
  }
];


class LoadMoreList extends React.Component {
  state = {
    loading: true,
    loadingMore: false,
    showLoadingMore: false,
    data: [],
    activitydata : [],
    activitygroups : [],
    lastKey : null,
    pagingCount : 25,
    edit_visible : false,
    editData : null,
    disableUserId : null,
    chosenDescription : null,
    tip : '',
    chosenFile : '',
    comment : '',
    filecomment : false,

    meetstart: null,
    meetend : null,
    attenders : [],

    //for supervisor
    currentKey : null

  }

  constructor(){
    super();
    this.dbops = new dbops();
  }

  componentDidMount() {
    this.getInitialData();
  }

  getInitialData = () => {
    this.getTeamActivity((res,res2) => {

      let rs = res;
      this.setState({
        loading: false,
        data: rs,
        activitygroups : res2
      });
    });
  }

  getTeamActivity = (callback) => {
    // console.log(this.props.projid);
    //debugger;
    // console.log(this.dbops.get_current_user())
    //collection, params, limit, lastKey, orderByKey
    this.setState({ loading : true })
    this.dbops.queryFS(Config.NETWORK.teamActivity,
      [{
        key : "projectid",
        comp : "==",
        val : this.props.projid.projectid
      }], this.state.pagingCount, null, 'time', (querySnapshot)=>{

        this.setState({ loading : false })
      if(!querySnapshot)
        callback([])

      let t = [],itemsArr = [],testGarr =[], groupsArr = [], ind = 0;

      querySnapshot.forEach(function(doc) {
        let ojbm = doc.data();

        ojbm.key = doc.id;

        let temp = {
          id: ind,
          group: ojbm.studentid,
          title: ojbm.comment ? ojbm.comment : 'File : '+moment(ojbm.time).format('llll'),
          start_time: ojbm.type =="meeting" ? moment(ojbm.meetstart): moment(ojbm.time),
          end_time: ojbm.type =="meeting" ? moment(ojbm.meetstart): moment(ojbm.time).add(2, 'hour'),
          item : ojbm
        }
        itemsArr.push(temp)
        t.push(ojbm);

        if(testGarr.indexOf(ojbm.studentid) < 0){
          testGarr.push(ojbm.studentid);
          groupsArr.push({ id: ojbm.studentid, title: ojbm.studentname, activity : 1});
        }
        else {
          groupsArr[testGarr.indexOf(ojbm.studentid)] = { id: ojbm.studentid, title: ojbm.studentname+' ('+(groupsArr[testGarr.indexOf(ojbm.studentid)].activity+1)+')', activity : groupsArr[testGarr.indexOf(ojbm.studentid)].activity+1}
        }

          ind++;
      });
      // console.log("fetched activity", t, itemsArr, groupsArr);
      items = itemsArr;
      groups = groupsArr;
      callback(t, groupsArr);
    }, (err)=>{
      console.log(err);
      this.setState({ loading : false })
      this.dbops.error("Error fetching data");
    })

  }

  // savesubmission = ( filepath ) => {
  //   // save_data_fire_promise = (collection, doc, value)
  //   let subdata = {
  //     "projectid" : this.props.projid.projectid,
  //     "teamid"    : this.props.projid.teamid,
  //     "studentid" : this.dbops.get_current_user(),
  //     "uploadURL" : filepath,
  //     "type"      : "file",
  //     "studentname" : this.dbops.get_current_username(),
  //     "comment"   : this.state.comment
  //   };
  //   this.dbops.save_data_fire_promise(
  //       Config.NETWORK.teamActivity,
  //       this.props.projid.teamKey+"_"+((new Date()).getTime()),
  //       subdata
  //     ).then(()=>{
  //       this.dbops.success("File Uploaded Successfully");
  //       this.setState({ lastSubmission : subdata, fileupload : false, chosenFile : false, comment : false});
  //       this.getInitialData();
  //     }).catch(()=>{
  //       this.dbops.success("Submission failed");
  //     })
  // }


  savecomment = ( ) => {
    this.dbops.update_data_fire_promise(
        Config.NETWORK.teamActivity,
        this.state.currentKey,
        {
          supervisorcomment : this.state.comment,
          supervisorcommentedtime : (new Date()).getTime()
        }, true
      ).then(()=>{
        this.dbops.success("Commented Successfully Successful");
        this.setState({ lastSubmission : null, filecomment : false, comment : false, currentKey: null});
        this.getInitialData();
      }).catch(()=>{
        this.dbops.error("Commenting failed");
      })

  }


  // savemeeting = ( ) => {
  //   // save_data_fire_promise = (collection, doc, value)
  //
  //   if(!this.state.meetstart || !this.state.meetend)
  //   {
  //       this.dbops.error("Choose start and end");
  //       return;
  //   }
  //
  //   let subdata = {
  //     "projectid" : this.props.projid.projectid,
  //     "teamid"    : this.props.projid.teamid,
  //     "studentid" : this.dbops.get_current_user(),
  //     "type"      : "meeting",
  //     "studentname" : this.dbops.get_current_username(),
  //     "comment"   : this.state.comment,
  //     "meetstart" : this.state.meetstart.valueOf(),
  //     "meetend"   : this.state.meetend.valueOf(),
  //     "attenders" : this.state.attenders
  //   };
  //   this.dbops.save_data_fire_promise(
  //       Config.NETWORK.teamActivity,
  //       this.props.projid.teamKey+"_"+((new Date()).getTime()),
  //       subdata
  //     ).then(()=>{
  //       this.dbops.success("Comment Added Successfully");
  //       this.setState({ lastSubmission : subdata, filemeeting : false, comment : false, meetstart: null, meetend : null, attenders: []});
  //       this.getInitialData();
  //     }).catch(()=>{
  //       this.dbops.error("Commenting failed");
  //     })
  // }


  signoffmeeting = (key, torf) => {
    this.dbops.update_data_fire_promise(
        Config.NETWORK.teamActivity,
        key,
        {
          signoff : torf,
          signedoffBy : this.dbops.get_current_user()
        }, true
      ).then(()=>{
        this.dbops.success("SignOff Successful");
        //this.setState({ lastSubmission : subdata, filemeeting : false, comment : false, meetstart: null, meetend : null, attenders: []});
        this.getInitialData();
      }).catch(()=>{
        this.dbops.error("SignOff failed");
      })
  }



  render() {
    const { loading, loadingMore, showLoadingMore, data } = this.state;

    let children = [];
    for (let i = 0; i < this.props.teamMembers.length; i++) {
      // console.log(this.props.teamMembers[i]);
      children.push(<Option key={this.props.teamMembers[i].studentid}>{this.props.teamMembers[i].studentid}</Option>);
    }

    // console.log(this.props.teamMembers);
    // console.log(children);

    const props = {
      onChange(info) {
        if (info.file.status !== 'uploading') {
          console.log(info.file, info.fileList);
        }
        if (info.file.status === 'done') {
          message.success(`${info.file.name} file uploaded successfully`);
        } else if (info.file.status === 'error') {
          message.error(`${info.file.name} file upload failed.`);
        }
      },
    };

    return (
      <Row type="flex" justify="center" align="top">
      <Divider />

      <Head>
        <title>Project Activity</title>

        <link rel="stylesheet" type="text/css" href="https://namespace-ee.github.io/react-calendar-timeline-docs/demo.css"/>
      </Head>

      {/*
      <style jsx>{`${TimelineStyles}`}</style>
      */}

      <Col span={22}>
      <Spin spinning={this.state.loading}>


      <Modal
          title="Upload a Submission"
          visible={this.state.fileupload}
          onOk={()=>{
            this.setState({loading : true});
            this.dbops.uploadFile(this.state.chosenFile.originFileObj,
              Config.NETWORK.teamActivity+'/'+this.props.projid.teamKey+"_"+this.dbops.get_current_user()+"_"+((new Date()).getTime())+".pdf",
              (per)=>{
                // console.log(per)
                this.setState({tip : Math.floor(per)+"% completed"});
              },
              (r)=>{
                //debugger;
                this.setState({loading : false});
                // console.log('done', r);
                this.setState({tip : "Processing..."});
                this.savesubmission(r);

              }, (e)=>{
                this.setState({loading : false, tip: ''});
                console.log('erro ', e)
                this.dbops.success("Submission failed");
            });
          }}
          onCancel={()=>{
            this.setState({ fileupload : false, chosenFile : false, comment : false })
          }}
      >
        <Spin spinning={this.state.loading} tip={this.state.tip}>
        <p>Choose or Drag a pdf file you want to submit </p>
        <TextArea placeholder="Enter your comment" onChange={(e)=>{
          console.log(e.target.value);
          this.setState({comment : e.target.value})
        }} autosize />
        <br/><br/><br/>

        {!this.state.chosenFile &&
        <UploadComp icon="upload" message="upload " filetype="application/pdf" fileext="pdf" maxSize="5000000"
          onFile={(file)=>{
            // console.log(file);
            // (file, filepath, progressf, successf, errorf)
            this.setState({ chosenFile : file });

          }}/>
        }

        {this.state.chosenFile &&
          <Tag closable color="#2db7f5" onClose={()=>{
            this.setState({ chosenFile : false})
          }}>Chosen PDF File </Tag>
        }


          </Spin>
      </Modal>


      <Modal
          title="Comment"
          visible={this.state.filecomment}
          onOk={()=>{
            this.setState({loading : true});
            this.savecomment();
          }}
          onCancel={()=>{
            this.setState({ filecomment : false, comment : false })
          }}
      >
        <Spin spinning={this.state.loading} tip={this.state.tip}>
        <TextArea placeholder="Enter your comment"
        rows={4}
        autosize={{ minRows: 3, maxRows: 6 }}
        onChange={(e)=>{
          console.log(e.target.value);
          this.setState({comment : e.target.value})
        }}  />
        <br/><br/>
        </Spin>
      </Modal>


      <Modal
          title="Log a Meeting time"
          visible={this.state.filemeeting}
          onOk={()=>{
            this.setState({loading : true});
            this.savemeeting();
          }}
          onCancel={()=>{
            this.setState({ filemeeting : false, comment : false, meetstart: null, meetend : null, attenders: [] })
          }}
      >
        <Spin spinning={this.state.loading} tip={this.state.tip}>
          <h4><b>Select Meeting Start & End Time</b></h4>
          <RangePicker
            showTime={{ format: 'HH:mm' }}
            format="YYYY-MM-DD HH:mm"
            placeholder={['Start Time', 'End Time']}
            onChange={(value, dateString) => {
              // console.log('Selected Time: ', value);
              // console.log('Formatted Selected Time: ', dateString);

            }}
            onOk={(value)=>{
              console.log('onOk: ', value);
              this.setState({ meetstart : value[0], meetend : value[1] });
            }}
          />
          <br/><br/>
          <p>Select Members who attended the meeting</p>
          <Select
            mode="tags"
            style={{ width: '100%' }}
            placeholder=""
            onChange={(value)=>{
              // console.log(`selected ${value}`);
              this.setState({ attenders : value});
            }}
          >
            {children}
          </Select>
          <br/><br/>
          <p>Enter meeting details</p>
          <TextArea placeholder="Enter your comment"
          rows={4}
          autosize={{ minRows: 3, maxRows: 6 }}
          onChange={(e)=>{
            console.log(e.target.value);
            this.setState({comment : e.target.value})
          }}  />
          <br/><br/>
        </Spin>
      </Modal>


      <h3>Team Activity</h3>
      {/*
      <ButtonGroup>
        <Button icon="schedule" onClick={()=>{ this.setState({ filemeeting : true }) }}> Add Meeting Minutes</Button>
        <Button icon="file-pdf" onClick={()=>{ this.setState({ fileupload : true }) }}>File</Button>
        <Button icon="message" onClick={()=>{ this.setState({ filecomment : true }) }}>Comment</Button>
      </ButtonGroup>
      */}
      {this.state.data.length == 0 &&
      <div style={{ textAlign : 'center'}}>
        <Avatar
        src={'/static/images/list.svg'}
        shape={'square'}
        style={{ color: '#f56a00', width: '45px', height: '45px', opacity : 0.5, marginTop: '20px' }} />
        <br/><br/>
        <h5 style={{ color : '#C0392B'}}>No Activity Yet!</h5>
      </div>
      }

      {this.state.data.length > 0 &&
      <Tabs defaultActiveKey="1">

        <TabPane tab={<span><Icon type="time" />Timeline View</span>} key="1">
          <Timeline>
              {this.state.data.map((item,index)=>{
                return <TimelineEvent title={item.studentname}
                              bubbleStyle={item.type == "comment" ? { borderColor : '#19B5FE', backgroundColor: '#fff'} : ( item.type=="meeting"? { borderColor : '#F9690E', backgroundColor: '#fff'}: { backgroundColor: '#fff'}) }
                               createdAt={moment(item.time).format('llll')}
                               icon={<Icon type={item.type == "comment" ? 'message' :  ( item.type=="meeting"? 'team' : item.type) }/>}
                >
                  {item.type == "file" &&
                  <a href={item.uploadURL} target="_blank">
                  <Avatar src={'/static/images/pdf.svg'} style={{ width: '40px', height: '40px'}} /><br/>
                  </a>
                  }

                  {item.type != "meeting" &&
                    <p>{item.comment? item.comment : ''}</p>
                  }

                  {item.type == "meeting" &&
                  <div>

                  <Card style={{ width: '100%' }}>
                  <Meta
                      avatar={<Avatar shape="square" src={'/static/images/meeting.svg'} />}
                      title={moment(item.meetstart).format('llll')+' - '+moment(item.meetend).format('LT')}
                      description={item.comment}
                    />
                  {item.attenders &&
                    <div>
                      <br/>
                      <h6><b>Attended</b></h6>
                      {item.attenders.map((item,index)=>{
                        return <Badge count={'ID: '+item} style={{ backgroundColor: '#fff', color: '#000'}} />
                      })}
                    </div>
                  }
                  </Card>

                  <br/>
                  <Button type="danger" onClick={()=>{
                      this.signoffmeeting(item.key, item.signoff == undefined? true: (item.signoff === true ? false : true));
                  }}> {item.signoff == undefined? 'Add': (item.signoff === true ? 'Remove' : 'Add')} SignOff</Button>
                  </div>
                  }

                  {item.supervisorcomment &&
                  <Card style={{ width: '90%' }}>
                  <Meta
                      avatar={<Avatar size="small" src={'/static/images/boss.svg'} />}
                      description={item.supervisorcomment}
                    />
                  </Card>
                  }
                  <br/>

                  {item.type == "comment" &&
                  <Button type="danger" shape="circle" icon="message" onClick={()=>{
                      //this.signoffmeeting(item.key, item.signoff == undefined? true: (item.signoff === true ? false : true));
                      this.setState({ filecomment : true, currentKey : item.key });
                  }} />
                  }

                  {item.type == "file" &&
                  <Button type="danger" shape="circle" icon="message" onClick={()=>{
                      //this.signoffmeeting(item.key, item.signoff == undefined? true: (item.signoff === true ? false : true));
                      this.setState({ filecomment : true, currentKey : item.key });
                  }} />
                  }

                </TimelineEvent>
              })}
          </Timeline>
        </TabPane>

        <TabPane tab={<span><Icon type="schedule" />Calendar View</span>} key="2">
          <TimelineCalendar
            groups={groups}
            items={items}
            defaultTimeStart={moment().add(-20, 'days')}
            defaultTimeEnd={moment().add(10, 'days')}
          />
        </TabPane>


        <TabPane tab={<span><Icon type="file-pdf" />Files</span>} key="3">
          <div>
          {this.state.data.map((item,index)=>{
            if(item.type == "file")
            return <div style={{ borderBottom: '#eaeaea solid 1px', paddingBottom : '5px'}}><a href={item.uploadURL} target="_blank" ><br/>
              <Avatar shape="square" src={'/static/images/pdf.svg'} style={{ width: '40px', height: '40px', marginRight: '10px', float:'left'}} />
              <h6><b>Uploaded by <span style={{ color : '#BF55EC'}}>{item.studentname}</span></b></h6>
              <h6><b>Uploaded Time <span style={{ color : '#6C7A89'}}>{moment(item.time).format('L')+' '+moment(item.time).format('LT')}</span></b></h6>
              </a>
              <div style={{ clear: 'both' }}></div>
              </div>
            else {
              return;
            }
          })}
          </div>
        </TabPane>


      </Tabs>
    }




      {/*
      {this.state.data.length > 0 &&
        <div>
        <h3>Unit {this.props.projid.projectid} has {this.state.data.length} tasks</h3>

        <Row gutter={22} style={{ background: '#ECECEC', padding: '30px' }}>
        {this.state.data.map((item,index)=>{
          return <AssessmentItem key={index} item={item} />
        })}
        </Row>
        </div>
      }
      */}

      </Spin>
      </Col>
      </Row>

    );
  }
}

export default LoadMoreList;
