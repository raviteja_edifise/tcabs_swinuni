import React from 'react';


import { Form, Input, Tooltip, Icon, Cascader, Select, Row, Col,
  Checkbox, Button, AutoComplete,message, Modal, DatePicker, Radio, Badge, Spin } from 'antd';
import moment from 'moment';
import is from 'is_js';

// import { Menu, Icon, Badge, Form, Button, message, Modal } from 'antd';
const FormItem = Form.Item;
const Option = Select.Option;
const AutoCompleteOption = AutoComplete.Option;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

import Config from '../util/config';
import CustomError from '../util/errors/errors';
import { validateEmail, PhoneNumberValidate, formatPhoneNumber, mergeObjs, removeKey } from '../util/util';
import dbops from '../util/dbops';

class UploadComp extends React.Component{
  state = {
    data: null,
    loading : false,
    visible: false,
    confirmDirty: false,
    autoCompleteResult: [],
  }
  constructor(props){
      super(props);
      this.dbops = new dbops();
  }

  componentDidMount(){
  }

  setVisible = (boll) => {
    this.setState({ visible : boll });
  }

  editStudent = (data) => {
    this.setState({ data : data });
    this.setVisible(true);
  }

  onSave = () => {
    const { validateFieldsAndScroll } = this.props.form;
    // validateFields();
    validateFieldsAndScroll((err, values) => {
      if (!err) {

        // console.log('Received values of form: ', values.dob);

        let editObj = {
          firstname : values.firstname,
          lastname : values.lastname,
          address : values.address,
          gender : values.gender,
          contact : formatPhoneNumber(values.phone),
          dob : values.dob.toDate().valueOf(),
          // email : this.props.studentData.email,
          // picture : this.props.studentData.picture,
          // uid : this.props.studentData.uid
        }

        editObj = mergeObjs ( this.props.studentData, editObj);
        editObj = removeKey( editObj, "key");
        // console.log(editObj);

        this.setState({ loading: true })
        this.dbops.save_data(
          '/students/'+this.props.studentData.uid,
          editObj,
          (yes) => {
            this.setState({ loading: false });
            // message(Config.UI.save_success);
            if(this.props.onSave)
              this.props.onSave(editObj);
          },
          (err)=> {
            this.setState({ loading: false });
            // message(Config.UI.user_save_error);

          });

      }
    });

    // () => {
    //   if(this.props.onSave)
    //     this.props.onSave();
    // }
  }

    handleSubmit = (e) => {
      e.preventDefault();
      this.props.form.validateFieldsAndScroll((err, values) => {
        if (!err) {
          console.log('Received values of form: ', values);
        }
      });
    }

  render(){
    const { getFieldDecorator } = this.props.form;
    const { autoCompleteResult } = this.state;
    let stuData = this.props.studentData ? this.props.studentData : null;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 16,
          offset: 8,
        },
      },
    };
    const prefixSelector = getFieldDecorator('prefix', {
      initialValue: '61',
    })(
      <Select style={{ width: 70 }}>
        <Option value="61">+61</Option>
      </Select>
    );

    const websiteOptions = autoCompleteResult.map(website => (
      <AutoCompleteOption key={website}>{website}</AutoCompleteOption>
    ));

      return (
          <div>
          <Spin spinning={this.state.loading} >
          {/* <style jsx global>{MenuCSS}</style> */}

          <Modal
            title="Edit Employee"
            wrapClassName="vertical-center-modal"
            okText=" Save "
            visible={!!this.props.studentData}
            onOk={this.onSave}
            onCancel={() => {
              if(this.props.onCancel)
                this.props.onCancel();
            }}
          >

          {this.props.studentData &&
          <Form className={' marLR25'} onSubmit={this.handleSubmit}>

          <p className={' center'}>
            <Badge count={0} dot>
              User Email : {stuData.email}
            </Badge>
          </p>


          <FormItem
            {...formItemLayout}
            label={(
              <span>
                First Name&nbsp;
                <Tooltip title="Your common name">
                  <Icon type="question-circle-o" />
                </Tooltip>
              </span>
            )}
          >
            {getFieldDecorator('firstname', {
              initialValue : stuData.firstname ? stuData.firstname : '',
              rules: [{ required: true, message: 'Please input your firstname!', whitespace: true },
              { validator : (rule, value, callback)=> {
                //const { getFieldValue } = this.props.form;
                let firstname = value;

                if( (''+firstname).length > 3 )
                {
                  if(is.string(firstname))
                    callback();
                  else
                    callback('Name can only contain alphabets');
                }
                callback('Enter your name');
              }}
            ],
            })(
              <Input />
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label={(
              <span>
                Last Name&nbsp;
                <Tooltip title="Your Surname">
                  <Icon type="question-circle-o" />
                </Tooltip>
              </span>
            )}
          >
            {getFieldDecorator('lastname', {
              initialValue : stuData.lastname ? stuData.lastname : '',
              rules: [{ required: true, message: 'Please input your lastname!', whitespace: true }],
            })(
              <Input />
            )}
          </FormItem>

            <FormItem
              {...formItemLayout}
              label="Date of birth"
            >
              {getFieldDecorator('dob', {
                  initialValue : moment(stuData.dob ? stuData.dob/1000 : ''),
                  rules: [{ type: 'object', required: true, message: 'Please select time!' },
                  { validator : (rule, value, callback)=> {
                    //const { getFieldValue } = this.props.form;
                    console.log(value);
                    let now = moment(new Date());
                    let chosen = moment(value);

                    let yrsdiff = chosen.diff(now, 'years');
                    if(yrsdiff >= 0){
                      callback('Date cannot be from future');
                    }else if(yrsdiff > -12 && yrsdiff < 0)
                    {
                      callback('Too young date chosen');
                    }else {
                      callback();
                    }


                    callback();
                    // if( (''+firstname).length > 3 )
                    // {
                    //   if(is.string(firstname))
                    //     callback();
                    //   else
                    //     callback('Name can only contain alphabets');
                    // }
                    // callback('Enter your name');
                  }}
                ],
                })(
                <DatePicker />
              )}
            </FormItem>

            <FormItem
              {...formItemLayout}
              label={(
                <span>
                  Address&nbsp;
                  <Tooltip title="Current address?">
                    <Icon type="question-circle-o" />
                  </Tooltip>
                </span>
              )}
            >
              {getFieldDecorator('address', {
                initialValue : stuData.address ? stuData.address : '',
                rules: [{ required: true, message: 'Please enter address', whitespace: true },
                { validator : (rule, value, callback)=> {
                  let regEx = /^[a-zA-Z0-9\s,'-]*$/;
                  if( is.not.empty(value) && value.length > 4 )
                  {
                    if(regEx.test(value))
                      callback();
                    else
                      callback('Address cannot contain special characters');
                  }
                  callback('Enter address ');
                }}],
              })(
                <Input />
              )}
            </FormItem>

            <FormItem
              {...formItemLayout}
              label="Phone Number"
            >
              {getFieldDecorator('phone', {
                initialValue : stuData.contact ? stuData.contact.replace("+61","") : '',
                rules: [{ required: true, message: 'Please input your phone number!' },
                { validator : (rule, value, callback)=> {
                  //const { getFieldValue } = this.props.form;
                  let pNumber = value;
                  pNumber = formatPhoneNumber(pNumber);
                  // console.log(pNumber);

                  if( (''+pNumber).length > 8 )
                  {
                    if(PhoneNumberValidate(pNumber))
                      callback();
                    else
                      callback('Invalid phone Number');
                  }
                  callback('Enter valid Number');
                }}],
              })(
                <Input addonBefore={prefixSelector} style={{ width: '100%' }} />
              )}
            </FormItem>

            <FormItem
              {...formItemLayout}
              label="Gender"
            >
            {getFieldDecorator('gender',{
              initialValue : stuData.lastname ? stuData.gender : 'Male'})(
              <RadioGroup >
                <Radio value="Male">Male</Radio>
                <Radio value="Female">Female</Radio>
              </RadioGroup>
            )}
            </FormItem>


          </Form>
          }
        </Modal>
        </Spin>

          </div>
      );
  }
}

const UploadCompManage = Form.create()(UploadComp);
export default UploadCompManage;
