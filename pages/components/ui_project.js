import React from 'react';
import { List, Avatar, Button, Spin, Modal, message, Badge, Col, Row } from 'antd';
const confirm = Modal.confirm
import reqwest from 'reqwest';

import Iframe from 'react-iframe';

import EditStudent from './edit_employee';
import dbops from '../util/dbops';
import Config from '../util/config';
import { mergeObjs, getIndexOfAttrInObj, openInNewTab } from '../util/util';

class StudentInfo extends React.Component {
  constructor(){
    super();
    this.dbops = new dbops();
    this.state = { name : ''}
  }
  componentDidMount() {
    console.log(this.props.uid);
    if(!this.props.uid)
      return;

    this.dbops.axiosreq(Config.NETWORK.userinfo , {
      'userId' : this.props.uid
    })
    .then( (response) => {
      console.log(response);
      this.setState({ name : response.data.user.displayName});

      if(this.props.onNameFetched)
        this.props.onNameFetched(response.data.user.displayName);

    })
    .catch( (err) => {
      console.log(err);
    });

  }
  render() {
    return (<div>
      <p> &nbsp;&nbsp;NAME : {(this.state.name)}</p>
    </div>)
  }
}

class LoadMoreList extends React.Component {
  state = {
    loading: true,
    projinfo : null,
    pagingCount : 25,
    teamMembers : []
  }

  constructor(){
    super();
    this.dbops = new dbops();
  }
  componentDidMount() {

    this.getProjectInfo();
    this.getTeamInfo((r)=>{
      this.setState({ teamMembers : r})

      if(this.props.onTeam)
        this.props.onTeam(r);

    });
  }

  getProjectInfo = () => {
    // debugger;
    this.dbops.get_data_fire(Config.NETWORK.convenorAddProject, this.props.project.projectid).then(r => {
      //console.log(r.data());
      this.setState({ projinfo : r.data()})
    })

  }

  getTeamInfo = (callback) =>{
    // this.dbops.get_data_fire(Config.NETWORK.convenorAddProject, this.props.project.teamKey).then(r => {
    //   //console.log(r.data());
    //   this.setState({ projinfo : r.data()})
    // })

    this.dbops.queryFS(Config.NETWORK.convenorAddTeamMember,
      [{
        key : "teamid",
        comp : "==",
        val : this.props.project.teamid
      }], this.state.pagingCount, null, null, (querySnapshot)=>{
      let t = [];

      querySnapshot.forEach(function(doc) {
        let ojbm = doc.data();

        ojbm.key = doc.id;
        t.push(ojbm);
      });
      console.log("fetched teamMembers", t);
      callback(t);
    }, (err)=>{
      console.log(err);
      this.dbops.error("Error fetching data");
    })

  }

  render() {
    return (
      <Row type="flex" justify="center" align="top">
      {this.state.projinfo &&
        <Col span={22}>

          <Col span={12}>
          <h3>Project {this.state.projinfo.projectid}</h3>
          <Badge count={'Unit ID: '+this.state.projinfo.unitid} style={{ backgroundColor: '#fff', color: '#CCC' }} />
          <br/><br/>

          <Button type="normal" onClick={()=>{
            openInNewTab(this.state.projinfo.projectdescription);
          }}>
            View Description
          </Button>
          </Col>

          <Col span={12}>
          <br/>
          <h3><b> Team : {this.props.project.teamid} </b></h3>
          {this.state.teamMembers.map((item,index) => {
            return <Col span={12}>
              <Badge count={'Student ID : '+item.studentid} style={{backgroundColor: '#fff', color: '#999'}} />
              <StudentInfo uid={item.studentid} onNameFetched={(name)=>{
                let tobj = this.state.teamMembers[index];
                tobj.name = name;
                let team = this.state.teamMembers;
                team[index] = tobj;
                this.setState({ teamMembers : team });

                if(this.props.onTeam)
                  this.props.onTeam(team);
              }}/>
            </Col>
          })}
          </Col>

<br/><br/>


        </Col>
      }
      </Row>

    );
  }
}

export default LoadMoreList;
