import React from 'react';


import { Form, Input, Tooltip, Icon, Cascader, Select, Row, Col,
  Checkbox, Button, AutoComplete,message, Modal, DatePicker, Radio, Badge, Spin } from 'antd';
import moment from 'moment';
import is from 'is_js';
// import LzEditor from 'react-lz-editor';
// import { EditorState } from 'draft-js';
// import { Editor } from 'react-draft-wysiwyg';
// import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';


// import BraftEditor from 'braft-editor';

// import { Menu, Icon, Badge, Form, Button, message, Modal } from 'antd';
const FormItem = Form.Item;
const Option = Select.Option;
const AutoCompleteOption = AutoComplete.Option;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

import Config from '../util/config';
import CustomError from '../util/errors/errors';
import { validateEmail, PhoneNumberValidate, formatPhoneNumber, mergeObjs, removeKey } from '../util/util';
import dbops from '../util/dbops';

class UploadComp extends React.Component{
  state = {
    data: null,
    loading : false,
    visible: false,
    confirmDirty: false,
    autoCompleteResult: [],

    // htmlContent: `<h1>Yankees, Peeking at the Red Sox, Will Soon Get an Eyeful</h1>
    //           <p>Whenever Girardi stole a glance, there was rarely any good news for the Yankees. While Girardi’s charges were clawing their way to a split of their four-game series against the formidable Indians, the Boston Red Sox were plowing past the rebuilding Chicago White Sox, sweeping four games at Fenway Park.</p>`,
    // markdownContent: "## HEAD 2 \n markdown examples \n ``` welcome ```",
    // responseList: []

  }
  constructor(props){
      super(props);
      this.dbops = new dbops();
  }

  componentDidMount(){
  }

  receiveHtml =(content) => {
    console.log("recieved HTML content", content);
    this.setState({responseList:[]});
  }

  setVisible = (boll) => {
    this.setState({ visible : boll });
  }

  editStudent = (data) => {
    this.setState({ data : data });
    this.setVisible(true);
  }

  onSave = (e) => {
    e.preventDefault();
    const { validateFieldsAndScroll, validateFields, resetFields } = this.props.form;

    validateFields(['aid', 'aname', 'adescription', 'dueDateTime', 'searchId', 'searchId2'], { force : true },(err, values) => {
      if (!err) {
        // console.log('Received values of form: ', values.dob);

        let editObj = {
          aid : values.aid,
          aname : values.aname,
          adescription : values.adescription,
          due : values.dueDateTime.valueOf(),
          unitid : values.searchId,
          supervisorid : values.searchId2
        }

        this.setState({ loading: true })
        //this.dbops.addUser
        //(collection, doc, value,
        this.checkAssessment(values.aid, (r)=>{
          // alert(JSON.stringify(r));

          if(r.length > 0 && !this.props.editData)
          {
            this.setState({ loading: false })
            this.dbops.error('Assessment ID already Exists');
            return;
          }

        this.dbops.save_data_fire(Config.NETWORK.convenorAssessment, values.aid, editObj,(resp)=>{
          this.dbops.success('Assessment Added successfully');
          this.setState({ loading : false });
          if(this.props.onSave)
              this.props.onSave();
          resetFields();
          // this.dbops.redirect(Config.url_admin);
        }, (err) => {
          debugger;
          console.log(err);
          this.dbops.error('Error in operation');
          this.setState({ loading : false });
        });
      });


      }
    });

    // () => {
    //   if(this.props.onSave)
    //     this.props.onSave();
    // }
  }


  checkAssessment = (value, callback) => {
    if(value.length > 3 ){
      this.setState({ lastSearch : value, dataSource : []});


      //autocomplete_fire = (collection, queryKey, queryString, limit, callback, error)
      //this.dbops.search_fire(Config.NETWORK.convenorTeam, 'teamid', '==', value, 2, (querySnapshot)=>{
      this.dbops.queryFS(Config.NETWORK.convenorAssessment,
        [{
          key : "aid",
          comp : "==",
          val : value
        }], 2, null, null, (querySnapshot)=>{
        let t = [];
        querySnapshot.forEach(function(doc) {
          // console.log(doc.id, " => ", doc.data());
          t.push(doc.id);
        });

        callback(t);
        // this.setState({ dataSource : t});
        // console.log(child);
      }, (err)=>{
        console.log(err);
        this.dbops.error("Error fetching data");
      })
    }
  }


  handleSearch = (value) => {
    if(value.length <= 4)
    {
      this.setState({
        dataSource : []
      })
    }

    if(value.length > 4 && value != this.state.lastSearch){
      this.setState({ lastSearch : value, dataSource : []});

      //autocomplete_fire = (collection, queryKey, queryString, limit, callback, error)
      this.dbops.autocomplete_fire(Config.NETWORK.adminAddUnitFire, 'unitid', value, 2, (querySnapshot)=>{
        let t = [];
        querySnapshot.forEach(function(doc) {
          // console.log(doc.id, " => ", doc.data());
          t.push(doc.id);
        });

        // callback(t);
        this.setState({ dataSource : t});
        // console.log(child);
      }, (err)=>{
        console.log(err);
        this.dbops.error("Error fetching data");
      })

      // this.dbops.autocomplete('/students/', value, (child)=>{
      //   let t = this.state.dataSource;
      //   t.push(child.id);
      //   this.setState({ dataSource : t});
      //   // console.log(child);
      // }, (err)=>{
      //   this.dbops.error("Error fetching data");
      // })


    }
  }

  onSelect = (value) => {
    let { setFieldsValue } = this.props.form;
    setFieldsValue({ searchId : value})
    // console.log('onSelect', value);
    // this.setState({ searchId : value})
  }

    handleSubmit = (e) => {
      e.preventDefault();
      const { validateFieldsAndScroll, validateFields } = this.props.form;
      validateFields(['uid', 'email', 'firstname', 'lastname', 'address', 'phone', 'dob', 'submit'], { force : true },(err, values) => {
        if (!err) {
          console.log('Received values of form: ', values);
        }
      });
    }

  supervisorSearch = (value, callback) => {
    if(value.length <= 4)
    {
      this.setState({
        dataSource : []
      })
    }

    if(value.length > 4 && value != this.state.lastSearch2){
      this.setState({ lastSearch2 : value, dataSource2 : []});
      //autocomplete_fire = (collection, queryKey, queryString, limit, callback, error)
      this.dbops.autocomplete_fire('supervisor', 'uid', value, 2, (querySnapshot)=>{
        let t = [];
        querySnapshot.forEach(function(doc) {
          console.log(doc.id, " => ", doc.data());
          t.push(doc.id);
        });
        this.setState({ dataSource2 : t});
      }, (err)=>{this.dbops.error("Error fetching data");})
    }
  }

  onSupervisorSelect = (value) => {
    let { setFieldsValue } = this.props.form;
    setFieldsValue({ searchId2 : value})
    // console.log('onSelect', value);
    // this.setState({ searchId : value})
  }

  render(){
    const { getFieldDecorator } = this.props.form;
    const { autoCompleteResult } = this.state;
    const { dataSource, dataSource2 } = this.state;
    let projData = this.props.editData ? this.props.editData : null;

    const { editorState } = this.state;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 16,
          offset: 8,
        },
      },
    };
    const config = {
          initialValue : projData ? moment(projData.due) : '',
          rules: [{ type: 'object', required: true, message: 'Please select time!' }],
        };
      return (
          <div>

          {/* <style jsx global>{MenuCSS}</style> */}

          <Modal
            title="Add Assessment"
            wrapClassName="vertical-center-modal"
            okText=" Save "
            visible={!!this.props.editData || this.props.visible}
            onOk={this.onSave}
            onCancel={() => {
              if(this.props.onCancel)
                this.props.onCancel();
            }}
          >
          <Spin spinning={this.state.loading} >

          {(this.props.editData || this.props.visible) &&
          <Form className={' marLR25'} onSubmit={this.handleSubmit}>

          <FormItem
            {...formItemLayout}
            label={(
              <span>
                Assessment ID&nbsp;
              </span>
            )}
          >
            {getFieldDecorator('aid', {
              initialValue : projData ? projData.aid : '',
              rules: [{ required: true, message: 'Please enter project ID!', whitespace: false },
              { validator : (rule, value, callback)=> {
                let unitid = value;

                if( (''+unitid).length >3 && (''+unitid).length < 9 )
                {
                  if(is.alphaNumeric(unitid) )
                    callback();
                  else
                    callback('Cannot have special characters');
                }
                callback(' Ex : A001 && no spaces && length 4 to 8 letters');
              }}
            ],
            })(
              <Input disabled={projData ? true : false} />
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label={(
              <span>
                 Unit ID&nbsp;
              </span>
            )}
          >
            {getFieldDecorator('searchId', {
              initialValue : projData ? projData.unitid : (this.state.searchId ? this.state.searchId : ''),
              rules: [{ required: true, message: 'cant be empty!', whitespace: false },
              { validator : (rule, value, callback)=> {
                //const { getFieldValue } = this.props.form;
                let unitid = value;

                if( (''+unitid).length >3 && (''+unitid).length <= 11 )
                {
                  if(/^[A-Za-z0-9 ]+$/.test(unitid) )
                    callback();
                  else
                    callback('Cannot have special characters');
                }
                callback(' Ex : 1010220332 && no spaces && length 4 to 8 letters');
              }}
            ],
            })(
              <AutoComplete
                disabled={projData ? true : false}
                dataSource={dataSource}
                style={{ width: 200 }}
                onSelect={this.onSelect}
                onSearch={this.handleSearch}
                placeholder="Ex: INF80015"
              />
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label={(
              <span>
                 Supervisor ID&nbsp;
              </span>
            )}
          >
            {getFieldDecorator('searchId2', {
              initialValue : projData ? projData.supervisorid : (this.state.searchId2 ? this.state.searchId2 : ''),
              rules: [{ required: true, message: 'cant be empty!', whitespace: false },
              { validator : (rule, value, callback)=> {
                //const { getFieldValue } = this.props.form;
                let unitid = value;

                if( (''+unitid).length >3 && (''+unitid).length <= 11 )
                {
                  if(!isNaN(parseInt(unitid)) )
                    callback();
                  else
                    callback('Cannot have special characters');
                }
                callback(' Ex : 1010220332 && no spaces && length 4 to 8 letters');
              }}
            ],
            })(
              <AutoComplete
                dataSource={dataSource2}
                style={{ width: 200 }}
                onSelect={this.onSupervisorSelect}
                onSearch={this.supervisorSearch}
                placeholder="Search supervisor"
              />
            )}
          </FormItem>


          <FormItem
            {...formItemLayout}
            label={(
              <span>
                Assessment Title&nbsp;
              </span>
            )}
          >
            {getFieldDecorator('aname', {
              initialValue : projData ? projData.aname : '',
              rules: [{ required: true, message: 'Please enter assessment title!', whitespace: true },
              { validator : (rule, value, callback)=> {
                //const { getFieldValue } = this.props.form;
                let firstname = value;

                if( (''+firstname).length > 3 &&  (''+firstname).length < 250 )
                {
                  if(/^[A-Za-z0-9 ]+$/.test(firstname))
                    callback();
                  else
                    callback('Name can only contain alphabets & numbers');
                }
                callback('Enter assessment title');
              }}
            ],
            })(
              <Input />
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label={(
              <span>
                URL of description&nbsp;
              </span>
            )}
          >
            {getFieldDecorator('adescription', {
              initialValue : projData ? projData.adescription : '',
              rules: [{ required: true, message: 'Please enter link of assessment description!', whitespace: true },
              { validator : (rule, value, callback)=> {
                //const { getFieldValue } = this.props.form;
                let firstname = value;

                function checkURL(url) {
                    return(url.match(/\.(jpeg|jpg|gif|png)$/) != null);
                }

                if( (''+firstname).length > 3 &&  (''+firstname).length < 250 )
                {
                  //if(checkURL(firstname))

                  var regex = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
                  if(regex.test(firstname))
                    callback();
                  else
                    callback('Enter valid url of the PDF');
                }
                callback('Enter PDF');
              }}
            ],
            })(
              <Input />
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Due Date"
          >
            {getFieldDecorator('dueDateTime', config)(
              <DatePicker showTime format="YYYY-MM-DD HH:mm:ss" />
            )}
          </FormItem>


          {/*
          <LzEditor active={true}
          importContent={this.state.htmlContent}
          cbReceiver={this.receiveHtml}
          lang="en"/>
          */}


          </Form>
          }

          </Spin>
        </Modal>

          </div>
      );
  }
}

const UploadCompManage = Form.create()(UploadComp);
export default UploadCompManage;
