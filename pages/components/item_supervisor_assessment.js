import React from 'react';
import { List, Avatar, Button, Spin, Modal,InputNumber, message, Badge, Col, Row, Card, Icon,  Upload } from 'antd';
const { Meta } = Card;
const confirm = Modal.confirm
import reqwest from 'reqwest';

import Iframe from 'react-iframe';
import moment from 'moment';

import EditStudent from './edit_employee';
import dbops from '../util/dbops';
import Config from '../util/config';
import { mergeObjs, getIndexOfAttrInObj, openInNewTab } from '../util/util';
import UploadComp from './upload';

import CommentsUI from './ui_comments_supervisor_assessment';

class LoadMoreList extends React.Component {
  state = {
    loading: true,
    projinfo : null,
    chosenDescription : false,
    lastSubmission : false,
    tip: '',
    showComments : false,
    gradeinput : false,
    gradevalue : 50
  }

  constructor(){
    super();
    this.dbops = new dbops();
  }
  componentDidMount() {
//submissions
    this.getsubmission(this.props.item.aid+"_"+this.dbops.get_current_user());
  }

  getsubmission = (docid) => {
    // debugger;
    this.setState({loading : true});
    this.dbops.get_data_fire(Config.NETWORK.submissions, docid).then(r => {
      // console.log("sssd ",r.data());
      let fetchedDoc = r.data();
      if(fetchedDoc)
        this.setState({ lastSubmission : fetchedDoc, loading : false})
      else {
        this.setState({loading : false});
      }
    })
  }

  gradesubmission = ( ) => {
    // save_data_fire_promise = (collection, doc, value)
    let subdata = {
      "grade" : this.state.gradevalue
    };
    //update_data_fire_promise = (collection, doc, value) => {
    this.dbops.update_data_fire_promise(
        Config.NETWORK.submissions,
        this.props.item.key,
        subdata
      ).then(()=>{
        this.dbops.success("Graded Successfully");
        this.setState({ lastSubmission : subdata, gradeinput : false});
      }).catch(()=>{
        this.dbops.success("Submission failed");
      })
  }

  render() {
    const { loading } = this.state;

    let currentDate = moment();
    let dueDate = this.props.item.due;
    let { item } = this.props.item;

    return (
      <Col span={11} gutter={10} style={{ marginRight: '3%', marginBottom: '10px'}}

      >
      <Spin spinning={this.state.loading} tip={this.state.tip}>
        <Card title={this.props.item.aname} bordered={false}
        actions={[
          <Badge count={0} dot onClick={()=>{
            openInNewTab(this.props.item.uploadURL);
          }}>
            <Icon type="eye-o" /> &nbsp;View
          </Badge>,
          <Badge count={0} dot onClick={()=>{
            this.setState({ showComments : true });
          }}>
            <Icon type="message" /> &nbsp;Comments
          </Badge>,
          <Badge count={0} dot onClick={()=>{
            //this.gradesubmission();
            this.setState({ gradeinput : true });
          }}>
            <Icon type="check" /> &nbsp;Grade
          </Badge>
          ]}
        >
        ID : {this.props.item.studentid}
        <br/>
        TIME : <Badge count={moment(this.props.item.due).format('lll')} style={{ backgroundColor: '#52c41a' }} />
        <br/>
        Graded : {this.props.item.grade ? this.props.item.grade : 'Not graded'}
        <br/><br/>

        <CommentsUI
          show={this.state.showComments}
          onCancel={()=>{ this.setState({ showComments : false})}}
          assessment={this.props.item}
        />

        <Modal
          title="Grade Assessment"
          visible={this.state.gradeinput}
          onOk={()=>{
            this.gradesubmission();
            this.setState({ gradevalue : 50})
          }}
          onCancel={()=>{
            this.setState({ gradeinput : false})
          }}
        >
          <p>Enter a value between 1 & 100 </p>
          <InputNumber size="large" min={1} max={100} defaultValue={this.state.gradevalue} onChange={(e)=>{
            this.setState({ gradevalue : e})
          }} />
        </Modal>


        </Card>

        </Spin>
      </Col>
    );
  }
}

export default LoadMoreList;
