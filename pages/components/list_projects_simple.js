import React from 'react';
import { List, Avatar, Button, Spin, Modal, message, Badge, Col, Row } from 'antd';
const confirm = Modal.confirm
import reqwest from 'reqwest';

import EditStudent from './edit_employee';
import dbops from '../util/dbops';
import Config from '../util/config';
import { mergeObjs, getIndexOfAttrInObj } from '../util/util';

const fakeDataUrl = 'https://randomuser.me/api/?results=5&inc=name,gender,email,picture,phone';

class LoadMoreList extends React.Component {
  state = {
    loading: true,
    loadingMore: false,
    showLoadingMore: true,
    data: [],
    lastKey : null,
    pagingCount : 50,
  }

  constructor(){
    super();
    this.dbops = new dbops();
  }
  componentDidMount() {

    this.getStudentsData((res) => {

      let rs = res;
      this.setState({
        loading: false,
        data: rs,
      });
    });
  }

  getStudentsData = (callback) => {
    // debugger;

    var path = 'projects';
    this.dbops.queryFS(path, [], this.state.pagingCount, this.state.lastKey, 'time', (resp)=>{
      let tempA = new Array();
      let firstTime = true;
      var templastKey = null;

      resp.forEach((doc) => {
        //console.log(doc.id, " => ", doc.data());
        let data = doc.data();
        data.key = doc.id;
        tempA.push(data);
        templastKey = tempA[tempA.length - 1].time
        firstTime = false;
      });
      this.setState({ lastKey : templastKey});
      if(tempA.length < this.state.pagingCount)
          this.setState({showLoadingMore : false});
      callback(tempA);
    }, (err)=>{
      console.log("errro ", err);
    })
      // var queryParams = {
      //       paging : this.state.pagingCount,
      //       lastKey : this.state.lastKey,
      //       orderBy : 'key',
      //       order: 'desc'
      //   };
      //
      //   var path = '/units/';
      //
      //   this.dbops.query(path, queryParams, (resp)=>{
      //     // console.log(resp.val());
      //     // debugger;
      //     var dealsData = resp;
      //     let tempA = new Array();
      //
      //     if(!dealsData)
      //     {
      //       callback(tempA);
      //       return;
      //     }
      //
      //     dealsData.forEach((child) => {
      //
      //         let eachkey = child.key;
      //         if(eachkey === this.state.lastKey){}
      //         else {
      //           let data = child.val();
      //           data.key = eachkey;
      //           tempA.push(data);
      //         }
      //
      //     });
      //     tempA.reverse();
      //     var templastKey = null;
      //     if(tempA.length > 0)
      //       templastKey = tempA[tempA.length - 1].key;
      //
      //     // alert(templastKey);
      //     this.setState({ lastKey : templastKey});
      //     //
      //     // //alert(tempA.length);
      //     if(tempA.length < this.state.pagingCount )
      //         this.setState({showLoadingMore : false});
      //     callback(tempA);
      //
      //   }, (err)=> {
      //     message.error(Config.UI.database_error)
      //   })
  }


  onLoadMore = () => {

    this.setState({
      loadingMore: true,
    });
    this.getStudentsData((res) => {
      const data = this.state.data.concat(res);
      this.setState({
        data,
        loadingMore: false,
      }, () => {
        window.dispatchEvent(new Event('resize'));
      });
    });
  }

  render() {
    const { loading, loadingMore, showLoadingMore, data } = this.state;
    const loadMore = showLoadingMore ? (
      <div style={{ textAlign: 'center', marginTop: 12, height: 50, lineHeight: '50px' }}>
        {loadingMore && <Spin />}
        {!loadingMore && <Button className={' '} onClick={this.onLoadMore}>loading more</Button>}
      </div>
    ) : null;
    return (
      <Row type="flex" justify="center" align="top">
      <Col span={18}>

      <h3>All Projects</h3>
      <List
        className="demo-loadmore-list"
        loading={loading}
        itemLayout="horizontal"
        loadMore={loadMore}
        dataSource={data}
        renderItem={item => (
          <List.Item onClick={()=>{
            if(this.props.onTap)
              this.props.onTap(item);
          }} className ={' pointer'}>
            <List.Item.Meta
              title={<a href="#">{item.projectid}</a>}
              description={item.projectname}
            />
          </List.Item>
        )}
      />

      </Col>
      </Row>

    );
  }
}

export default LoadMoreList;
