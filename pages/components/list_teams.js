import React from 'react';
import { List, Avatar, Button, Spin, Modal, message, Badge, Col, Row } from 'antd';
const confirm = Modal.confirm
import reqwest from 'reqwest';
import moment from 'moment';
import Iframe from 'react-iframe';
import { Document, Page } from 'react-pdf';

import EditStudent from './edit_employee';
import dbops from '../util/dbops';
import Config from '../util/config';
import { mergeObjs, getIndexOfAttrInObj } from '../util/util';


import AddTeamMember from './add_team_member';
import AddTeam from './add_team';

const fakeDataUrl = 'https://randomuser.me/api/?results=5&inc=name,gender,email,picture,phone';

class LoadMoreList extends React.Component {
  state = {
    loading: true,
    loadingMore: false,
    showLoadingMore: true,
    data: [],
    lastKey : null,
    pagingCount : 10,
    edit_visible : false,
    editData : null,
    disableUserId : null,
    descVisible : false,
    chosenTeam : null,
    chosenTeamMembers : []
  }

  constructor(){
    super();
    this.dbops = new dbops();
  }
  componentDidMount() {

    this.getStudentsData((res) => {

      let rs = res;
      this.setState({
        loading: false,
        data: rs,
      });
    });
  }

  editStudentOpen = (data) => {
    // debugger;
    if(this.refs.editStudent)
      this.refs.editStudent.editStudent(true);
  }

  getStudentsData = (callback) => {
    // debugger;
    const chosenUnit = this.props.chosenUnit;
    if(!chosenUnit){

      callback([])
      return;
    }
    this.setState({loading: true});
    var path = Config.NETWORK.convenorTeam;
    this.dbops.queryFS(path, [
      {
        key : "projectid",
        comp : "==",
        val : chosenUnit.projectid
      }
    ], this.state.pagingCount, null, null, (resp)=>{
      this.setState({loading: false});
      let tempA = new Array();
      let firstTime = true;
      var templastKey = null;

      resp.forEach((doc) => {
        //console.log(doc.id, " => ", doc.data());
        let data = doc.data();
        data.key = doc.id;
        tempA.push(data);
        templastKey = tempA[tempA.length - 1].time
        firstTime = false;
      });
      this.setState({ lastKey : templastKey});
      if(tempA.length < this.state.pagingCount)
          this.setState({showLoadingMore : false});
      callback(tempA);
    }, (err)=>{
      console.log("errro ", err);
      this.setState({loading: false});
    })

  }


  onLoadMore = () => {

    this.setState({
      loadingMore: true,
    });
    this.getStudentsData((res) => {
      const data = this.state.data.concat(res);
      this.setState({
        data,
        loadingMore: false,
      }, () => {
        window.dispatchEvent(new Event('resize'));
      });
    });
  }

  removeTeam = (item, newstatus) => {
    // console.log(item.uid);
    confirm({
    title: 'Are you sure '+(newstatus == "active" ? 'enable' : 'disable')+' this unit?',
    content: '',
    okText: 'Yes',
    okType: 'danger',
    cancelText: 'No',
    onOk: () => {
      this.dbops.save_data(Config.NETWORK.adminAddUnit+'/'+item.unitid+'/status', newstatus,(resp)=>{
        this.dbops.success('Unit updated successfully');
        this.setState({ loading : false });
        this.refreshList();
      }, (err) => {
        this.dbops.error('Error in operation');
        this.setState({ loading : false });
      })
    },
    onCancel: () => {},
  });
  }

  refreshList = () => {

    this.setState({ data : [], lastKey : null})
    this.getStudentsData((res) => {
      let rs = res;
      this.setState({
        loading: false,
        data: rs,
      });
    });
  }

  loadteams = (item) => {
    this.loadTeam(item);
    // if(this.AddTeamMember)
    //   this.AddTeamMember.loadTeam()
    //   else {
    //     console.log('ss')
    //   }
  }

  loadTeam = (item) => {
    console.log('ocming')
    this.getTeamMembers(item, (res) => {
      let rs = res;
      console.log(rs);
      this.setState({
        chosenTeamMembers: rs
      });
    });
  }

  getTeamMembers = (item, callback) => {
    // debugger;

    // editObj.projectid = this.props.chosenTeam.projectid;
    // editObj.teamid = this.props.chosenTeam.teamid;
    // editObj.teamKey = this.props.chosenTeam.key;
    // editObj.studentid = values.searchId;

// console.log(item);
    let chosenTeam = item;

    if(!chosenTeam){
      callback([])
      return;
    }
    // console.log(this.state.chosenTeam);


    this.setState({loading: true});
    var path = Config.NETWORK.convenorAddTeamMember;
    this.dbops.queryFS(path,
      [{
        key : "teamid",
        comp : "==",
        val : chosenTeam.teamid
      },{
        key : "projectid",
        comp : "==",
        val : chosenTeam.projectid
      }], this.state.pagingCount, null, null, (resp)=>{
      this.setState({loading: false});
      let tempA = new Array();
      let firstTime = true;
      var templastKey = null;

      resp.forEach((doc) => {
        //console.log(doc.id, " => ", doc.data());
        let data = doc.data();
        data.key = doc.id;
        tempA.push(data);
        templastKey = tempA[tempA.length - 1].time
        firstTime = false;
      });
      // this.setState({ lastKey : templastKey});
      // if(tempA.length < this.state.pagingCount)
      //     this.setState({showLoadingMore : false});
      callback(tempA);
    }, (err)=>{
      console.log("errro ", err);
      this.setState({loading: false});
    })

  }


  getTeamSupervisor = (item, callback) => {
    let chosenTeam = item;

    this.setState({loading: true});
    var path = Config.NETWORK.convenorSupervisor;
    this.dbops.queryFS(path,
      [{
        key : "teamid",
        comp : "==",
        val : chosenTeam.teamid
      },{
        key : "projectid",
        comp : "==",
        val : chosenTeam.projectid
      }], this.state.pagingCount, null, null, (resp)=>{
      this.setState({loading: false});
      let tempA = new Array();
      let firstTime = true;
      var templastKey = null;

      resp.forEach((doc) => {
        //console.log(doc.id, " => ", doc.data());
        let data = doc.data();
        data.key = doc.id;
        tempA.push(data);
        templastKey = tempA[tempA.length - 1].time
        firstTime = false;
      });
      // this.setState({ lastKey : templastKey});
      // if(tempA.length < this.state.pagingCount)
      //     this.setState({showLoadingMore : false});
      callback(tempA);
    }, (err)=>{
      console.log("errro ", err);
      this.setState({loading: false});
    })

  }

  render() {

    const { loading, loadingMore, showLoadingMore, data } = this.state;
    const chosenUnit = this.props.chosenUnit;


    const loadMore = showLoadingMore ? (
      <div style={{ textAlign: 'center', marginTop: 12, height: 50, lineHeight: '50px' }}>
        {loadingMore && <Spin />}
        {!loadingMore && <Button className={' '} onClick={this.onLoadMore}>loading more</Button>}
      </div>
    ) : null;
    return (
      <Row type="flex" justify="center" align="top">
      <Col span={24}>

      {chosenUnit == null &&
        <div className={' center'}>
          <Avatar src ={'/static/images/pointing-left.svg'} className={'wh100px'}/>
          <div>Choose Project</div>
        </div>
      }

      <AddTeamMember onSave={this.refreshList}
        ref={(input) => {this.AddTeamMember = input}}
        chosenProject={this.props.chosenUnit}
        chosenTeam={this.state.chosenTeam}
        chosenTeamMembers={this.state.chosenTeamMembers}
        chosenTeamSupervisor={this.state.teamSupervisor}
        onCancel={()=>{
          this.setState({ chosenTeam : null})
        }}
         />

      {chosenUnit &&
        <div>
      <h3 className={'color1 '}>Enrolments for {chosenUnit.projectname}</h3>
      <h4>Unit ID : {chosenUnit.projectid}</h4>

      <Button type="primary" icon="open" onClick={()=>{
        this.setState({ descVisible : true });
      }} > Open Project Description </Button>

      <Modal
          visible={this.state.descVisible}
          width={'70%'}
          title={chosenUnit.projectname ? chosenUnit.projectname : ""}
          onOk={()=>{
            this.setState({ descVisible: false})
          }}
          onCancel={()=>{
            this.setState({ descVisible: false})
          }}
          footer={[
            <Button key="submit" type="primary" loading={loading} onClick={()=>{
              this.setState({ descVisible: false})
            }}>
              Close
            </Button>,
          ]}
        >

        {chosenUnit && this.state.descVisible &&
        <Iframe url={chosenUnit.projectdescription}
        width={'100%'}
        height="450px"
        id="myId"
        className="myClassname"
        display="initial"
        position="relative"
        allowFullScreen/>
        }
        {/*
        <Document
          file={'http://unec.edu.az/application/uploads/2014/12/pdf-sample.pdf'}
          onSourceError={(err)=> { console.log('err',err) }}
          onLoadSuccess={()=>{}}
        >
          <Page pageNumber={1} />
        </Document>
        */}
      </Modal>

      <Spin spinning={this.state.loading} >

      <AddTeam onSave={this.refreshList} chosenProject={this.props.chosenUnit} />

      <List
        className="demo-loadmore-list"
        loading={loading}
        itemLayout="horizontal"
        loadMore={loadMore}
        dataSource={data}
        renderItem={item => (
          <List.Item actions={[
            <Button type="primary" icon="edit" onClick={
              ()=>{
                // // this.editStudentOpen(item);
                // let newstatus = "active";
                // if(item.status == "active")
                //   newstatus = "inactive";
                //

                // if(item.status == "inactive")
                //   newstatus = "active";
                //
                // this.disableUnit(item, newstatus);
                this.setState({ chosenTeam : item });
                this.loadteams(item);
                this.getTeamSupervisor(item, (res)=> {
                  this.setState({
                    teamSupervisor : res
                  })
                })

              }} >{'Manage'}</Button>,

            <Button type="danger" onClick={
              ()=>{
                // // this.editStudentOpen(item);
                let newstatus = "active";
                if(item.status == "active")
                  newstatus = "inactive";


                // if(item.status == "inactive")
                //   newstatus = "active";
                //
                // this.disableUnit(item, newstatus);
                this.removeTeam(item,newstatus);
              }} >{'Remove'}</Button>]}>
            <List.Item.Meta
              title={<a href="#">{item.teamid}</a>}
              description={moment(item.time).format('L')}
            />
          </List.Item>
        )}
      />
        </Spin>

        </div>
      }

      </Col>
      </Row>

    );
  }
}

export default LoadMoreList;
