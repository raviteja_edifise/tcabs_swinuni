import React from 'react';
import { List, Avatar, Button, Spin, Modal, Badge, message, Col, Row, Collapse, Card, Icon, Meta, Upload } from 'antd';
const Panel = Collapse.Panel;
const confirm = Modal.confirm
// import reqwest from 'reqwest';


import dbops from '../util/dbops';
import Config from '../util/config';
import { mergeObjs, getIndexOfAttrInObj } from '../util/util';
// import UploadComp from './upload';
import AssessmentItem from './item_supervisor_assessment';

import ProjectUI from './ui_project';

class LoadMoreList extends React.Component {
  state = {
    loading: true,
    loadingMore: false,
    showLoadingMore: false,
    data: [],
    lastKey : null,
    pagingCount : 25,
    edit_visible : false,
    editData : null,
    disableUserId : null,
    chosenDescription : null
  }

  constructor(){
    super();
    this.dbops = new dbops();
  }
  componentDidMount() {

    this.getStudentsData((res) => {

      let rs = res;
      this.setState({
        loading: false,
        data: rs,
      });
    });
  }

  getStudentsData = (callback) => {
    // console.log(this.props.unitid);
    //debugger;
    // console.log(this.dbops.get_current_user())
    //collection, params, limit, lastKey, orderByKey
    this.setState({ loading : true })
    this.dbops.queryFS("submissions",
      [{
        key : "aid",
        comp : "==",
        val : this.props.chosenaid
      }], this.state.pagingCount, null, null, (querySnapshot)=>{

        this.setState({ loading : false })
        if(!querySnapshot)
          callback([])

        let t = [];

        querySnapshot.forEach(function(doc) {
          let ojbm = doc.data();

          ojbm.key = doc.id;
          t.push(ojbm);
        });
        console.log("fetched records", t);
        callback(t);
    }, (err)=>{
      console.log(err);
      this.setState({ loading : false })
      this.dbops.error("Error fetching data");
    })

  }


  render() {
    const { loading, loadingMore, showLoadingMore, data } = this.state;

    return (
      <Row type="flex" justify="center" align="top">
      <Col span={22}>
      <Spin spinning={this.state.loading}>


      {this.state.data.length == 0 &&
      <div style={{ textAlign : 'center'}}>
        <Avatar
        src={'/static/images/student-working-at-desk.svg'}
        shape={'square'}
        style={{ color: '#f56a00', width: '100px', height: '100px' }} />
        <br/><br/>
        <h3>No Submissions Yet!!<br/>Alright, lets get a coffee :)</h3>
      </div>
      }

      {this.state.data.length > 0 &&
        <div>
        <h3> {this.state.data.length} Submission{this.state.data.length>0?'s':''} for Assessment {this.props.chosenaid}</h3>

        <Row gutter={22} style={{ background: '#ECECEC', padding: '30px' }}>
        {this.state.data.map((item,index)=>{
          return <AssessmentItem key={index} item={item} />
        })}
        </Row>
        </div>
      }


      </Spin>
      </Col>
      </Row>

    );
  }
}

export default LoadMoreList;
