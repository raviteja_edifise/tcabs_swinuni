import React from 'react';


import { Form, Input, Tooltip, Icon, Cascader, Select, Row, Col,
  Checkbox, Button, AutoComplete,message, Modal, DatePicker, Radio, Badge, Spin } from 'antd';
import moment from 'moment';
import is from 'is_js';

// import { Menu, Icon, Badge, Form, Button, message, Modal } from 'antd';
const FormItem = Form.Item;
const Option = Select.Option;
const AutoCompleteOption = AutoComplete.Option;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

import Config from '../util/config';
import CustomError from '../util/errors/errors';
import { validateEmail, PhoneNumberValidate, formatPhoneNumber, mergeObjs, removeKey } from '../util/util';
import dbops from '../util/dbops';

class UploadComp extends React.Component{
  state = {
    data: null,
    loading : false,
    visible: false,
    confirmDirty: false,
    autoCompleteResult: [],
  }
  constructor(props){
      super(props);
      this.dbops = new dbops();
  }

  componentDidMount(){
  }

  setVisible = (boll) => {
    this.setState({ visible : boll });
  }

  editStudent = (data) => {
    this.setState({ data : data });
    this.setVisible(true);
  }

  onSave = (e) => {
    e.preventDefault();
    const { validateFieldsAndScroll, validateFields } = this.props.form;

    //['uid', 'email', 'dob', 'address', 'contact', 'gender', 'firstname', 'lastname'], { force : true },
    // validateFields();
    validateFields(['uid', 'email', 'firstname', 'lastname', 'address', 'phone', 'dob', 'gender'], { force : true },(err, values) => {
      if (!err) {

        // console.log('Received values of form: ', values.dob);
        // debugger;

        let editObj = {
          uid : values.uid,
          email : values.email,
          firstname : values.firstname,
          lastname : values.lastname,
          address : values.address,
          gender : values.gender,
          phoneNumber : formatPhoneNumber(values.phone),
          dob : values.dob.valueOf(),
          // email : this.props.studentData.email,
          // picture : this.props.studentData.picture,
          // uid : this.props.studentData.uid
        }

        // return;
        // editObj = mergeObjs ( this.props.studentData, editObj);
        // editObj = removeKey( editObj, "key");
        // console.log(editObj);

        // console.log(editObj);
        // debugger;
        this.setState({ loading: true })
        //this.dbops.addUser
        this.dbops.addUser(Config.NETWORK.adminAddUser, editObj,(resp)=>{
          // console.log(resp);
          this.dbops.success('Successfully Done');
          this.setState({ loading : false });

          this.dbops.redirect(Config.url_admin);
        }, (err) => {
          // debugger;
          console.log(err);

          message.error('Error in operation');
          this.setState({ loading : false });
        })


      }
    });

    // () => {
    //   if(this.props.onSave)
    //     this.props.onSave();
    // }
  }

    handleSubmit = (e) => {
      e.preventDefault();
      const { validateFieldsAndScroll, validateFields } = this.props.form;
      validateFields(['uid', 'email', 'firstname', 'lastname', 'address', 'phone', 'dob', 'submit'], { force : true },(err, values) => {
        if (!err) {
          console.log('Received values of form: ', values);
        }
      });
    }

  render(){
    const { getFieldDecorator } = this.props.form;
    const { autoCompleteResult } = this.state;
    let stuData = this.props.studentData ? this.props.studentData : null;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 16,
          offset: 8,
        },
      },
    };
    const prefixSelector = getFieldDecorator('prefix', {
      initialValue: '61',
    })(
      <Select style={{ width: 70 }}>
        <Option value="61">+61</Option>
      </Select>
    );

    const websiteOptions = autoCompleteResult.map(website => (
      <AutoCompleteOption key={website}>{website}</AutoCompleteOption>
    ));

      return (
          <div>
          <Spin spinning={this.state.loading} >
          {/* <style jsx global>{MenuCSS}</style> */}

          <Modal
            title="Add Student"
            wrapClassName="vertical-center-modal"
            okText=" Save "
            visible={!!this.props.studentData}
            onOk={this.onSave}
            onCancel={() => {
              if(this.props.onCancel)
                this.props.onCancel();
            }}
          >

          {this.props.studentData &&
          <Form className={' marLR25'} onSubmit={this.handleSubmit}>

          <FormItem
            {...formItemLayout}
            label={(
              <span>
                Student ID&nbsp;
                <Tooltip title="Student ID">
                  <Icon type="question-circle-o" />
                </Tooltip>
              </span>
            )}
          >
            {getFieldDecorator('uid', {
              rules: [{ required: true, message: 'Please input student ID!', whitespace: false },
              { validator : (rule, value, callback)=> {
                //const { getFieldValue } = this.props.form;
                let studentid = value;

                if( (''+studentid).length > 5 )
                {
                  if(!isNaN(studentid))
                    callback();
                  else
                    callback('Student can only have numbers');
                }
                callback('Enter student ID');
              }}
            ],
            })(
              <Input />
            )}
          </FormItem>


          <FormItem
            {...formItemLayout}
            label={(
              <span>
                Email&nbsp;
                <Tooltip title="Student Email">
                  <Icon type="question-circle-o" />
                </Tooltip>
              </span>
            )}
          >
            {getFieldDecorator('email', {
              rules: [{ required: true, message: 'Please input email!', whitespace: false },
              { validator : (rule, value, callback)=> {
                //const { getFieldValue } = this.props.form;
                let email = value;

                if( (''+email).length > 5 )
                {
                  if(validateEmail(email))
                    callback();
                  else
                    callback('Email not valid');
                }
                callback('Enter email');
              }}
            ],
            })(
              <Input />
            )}
          </FormItem>


          <FormItem
            {...formItemLayout}
            label={(
              <span>
                First Name&nbsp;
                <Tooltip title="Your common name">
                  <Icon type="question-circle-o" />
                </Tooltip>
              </span>
            )}
          >
            {getFieldDecorator('firstname', {
              rules: [{ required: true, message: 'Please input your firstname!', whitespace: true },
              { validator : (rule, value, callback)=> {
                //const { getFieldValue } = this.props.form;
                let firstname = value;

                if( (''+firstname).length > 3 )
                {
                  if(is.string(firstname))
                    callback();
                  else
                    callback('Name can only contain alphabets');
                }
                callback('Enter your name');
              }}
            ],
            })(
              <Input />
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label={(
              <span>
                Last Name&nbsp;
                <Tooltip title="Your Surname">
                  <Icon type="question-circle-o" />
                </Tooltip>
              </span>
            )}
          >
            {getFieldDecorator('lastname', {
              rules: [{ required: true, message: 'Please input your lastname!', whitespace: true }],
            })(
              <Input />
            )}
          </FormItem>

            <FormItem
              {...formItemLayout}
              label="Date of birth"
            >
              {getFieldDecorator('dob', {
                  initialValue : moment(),
                  rules: [{ type: 'object', required: true, message: 'Please select time!' },
                  { validator : (rule, value, callback)=> {
                    //const { getFieldValue } = this.props.form;
                    // console.log(value);
                    let now = moment(new Date());
                    let chosen = moment(value);

                    let yrsdiff = chosen.diff(now, 'years');
                    if(yrsdiff >= 0){
                      callback('Date cannot be from future');
                    }else if(yrsdiff > -12 && yrsdiff < 0)
                    {
                      callback('Too young date chosen');
                    }else {
                      callback();
                    }


                    callback();
                    // if( (''+firstname).length > 3 )
                    // {
                    //   if(is.string(firstname))
                    //     callback();
                    //   else
                    //     callback('Name can only contain alphabets');
                    // }
                    // callback('Enter your name');
                  }}
                ],
                })(
                <DatePicker />
              )}
            </FormItem>

            <FormItem
              {...formItemLayout}
              label={(
                <span>
                  Address&nbsp;
                  <Tooltip title="Current address?">
                    <Icon type="question-circle-o" />
                  </Tooltip>
                </span>
              )}
            >
              {getFieldDecorator('address', {
                rules: [{ required: true, message: 'Please enter address', whitespace: true },
                { validator : (rule, value, callback)=> {
                  let regEx = /^[a-zA-Z0-9\s,'-]*$/;
                  if( is.not.empty(value) && is.existy(value) )
                  if(value.length > 4)
                  {
                    if(regEx.test(value))
                      callback();
                    else
                      callback('Address cannot contain special characters');
                  }
                  callback('Enter address ');
                }}],
              })(
                <Input />
              )}
            </FormItem>

            <FormItem
              {...formItemLayout}
              label="Phone Number"
            >
              {getFieldDecorator('phone', {
                rules: [{ required: true, message: 'Please input your phone number!' },
                { validator : (rule, value, callback)=> {
                  //const { getFieldValue } = this.props.form;
                  let pNumber = value;

                  if(!pNumber){
                  callback('Enter valid Number');
                  return;
                }
                  // console.log(pNumber);

                  if( (''+pNumber).length > 8 )
                  {
                    // debugger;
                    pNumber = formatPhoneNumber(pNumber);
                    if(PhoneNumberValidate(pNumber))
                      callback();
                    else
                      callback('Invalid phone Number');
                  }
                  callback('Enter valid Number');
                }}],
              })(
                <Input addonBefore={prefixSelector} style={{ width: '100%' }} />
              )}
            </FormItem>

            <FormItem
              {...formItemLayout}
              label="Gender"
            >
            {getFieldDecorator('gender',{
              initialValue : 'Male'})(
              <RadioGroup >
                <Radio value="Male">Male</Radio>
                <Radio value="Female">Female</Radio>
              </RadioGroup>
            )}
            </FormItem>

          </Form>
          }
        </Modal>
        </Spin>

          </div>
      );
  }
}

const UploadCompManage = Form.create()(UploadComp);
export default UploadCompManage;
