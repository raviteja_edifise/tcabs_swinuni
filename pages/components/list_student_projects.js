import React from 'react';
import { List, Avatar, Button, Spin, Modal, message, Badge, Col, Row, Collapse } from 'antd';
const Panel = Collapse.Panel;
const confirm = Modal.confirm
// import reqwest from 'reqwest';

import dbops from '../util/dbops';
import Config from '../util/config';
import { mergeObjs, getIndexOfAttrInObj } from '../util/util';

import ProjectUI from './ui_project';

const fakeDataUrl = 'https://randomuser.me/api/?results=5&inc=name,gender,email,picture,phone';

class LoadMoreList extends React.Component {
  state = {
    loading: true,
    loadingMore: false,
    showLoadingMore: false,
    data: [],
    lastKey : null,
    pagingCount : 25,
    edit_visible : false,
    editData : null,
    disableUserId : null
  }

  constructor(){
    super();
    this.dbops = new dbops();
  }
  componentDidMount() {

    this.getStudentsData((res) => {

      let rs = res;
      this.setState({
        loading: false,
        data: rs,
      });
    });
  }

  getStudentsData = (callback) => {
    // debugger;
    console.log(this.dbops.get_current_user())
    this.dbops.queryFS(Config.NETWORK.convenorAddTeamMember,
      [{
        key : "studentid",
        comp : "==",
        val : this.dbops.get_current_user()
      }], this.state.pagingCount, null, null, (querySnapshot)=>{
      let t = [];

      querySnapshot.forEach(function(doc) {
        let ojbm = doc.data();

        ojbm.key = doc.id;
        t.push(ojbm);
      });
      console.log("fetched records", t);
      callback(t);
    }, (err)=>{
      console.log(err);
      this.dbops.error("Error fetching data");
    })

  }


  render() {
    const { loading, loadingMore, showLoadingMore, data } = this.state;
    const loadMore = showLoadingMore ? (
      <div style={{ textAlign: 'center', marginTop: 12, height: 50, lineHeight: '50px' }}>
        {loadingMore && <Spin />}
        {!loadingMore && <Button className={' '} onClick={this.onLoadMore}>loading more</Button>}
      </div>
    ) : null;
    return (
      <Row type="flex" justify="center" align="top">
      <Col span={18}>

      <h3>My Projects</h3>

      <Collapse defaultActiveKey={['1']} onChange={this.projectChanged}>
        {this.state.data.map((item,index)=>{
          return <Panel header={'PROJECT ID : '+item.projectid+'   TEAM ID : '+item.teamid} key={index}>
            <div>
              <ProjectUI projid={item.projectid} />
            </div>
          </Panel>
        })}
      </Collapse>

      </Col>
      </Row>

    );
  }
}

export default LoadMoreList;
