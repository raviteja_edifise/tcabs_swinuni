import React from 'react';
import { List, Avatar, Button, Spin, Modal, Icon, message, Badge, Col, Row, Input, Breadcrumb} from 'antd';
const { TextArea } = Input;
const confirm = Modal.confirm
import reqwest from 'reqwest';
import moment from 'moment';

import Iframe from 'react-iframe';

import EditStudent from './edit_employee';
import dbops from '../util/dbops';
import Config from '../util/config';
import { mergeObjs, getIndexOfAttrInObj, sortByKey, sortArraybyTime } from '../util/util';


class LoadMoreList extends React.Component {
  state = {
    loading: true,
    msgbox : '',
    pagingCount : 25,
    comments : [
        // {
        //   comment: 'Ant Design Title 4',
        // },
      ]
  }

  constructor(){
    super();
    this.dbops = new dbops();
  }

  updateComments = (r) => {
      let newarr = sortArraybyTime(r);
      console.log("ssss ",newarr)
      this.setState({ comments : newarr.reverse() });
  }

  componentDidMount() {

    this.getComments( (r)=> {
      this.updateComments(r);
      this.getRealTimeUpdate();
    });

  }

  getComments = (callback) => {
    // debugger;

    this.dbops.queryFS(
      Config.NETWORK.submissions+'/'+this.props.assessment.key+'/comments/',
      [], this.state.pagingCount, null, null, (querySnapshot)=>{

        this.setState({ loading : false })
      if(!querySnapshot)
        callback([])

      let t = [];

      querySnapshot.forEach(function(doc) {
        let ojbm = doc.data();

        ojbm.key = doc.id;
        t.push(ojbm);
      });
      console.log("fetched comments", t);
      callback(t);
    }, (err)=>{
      console.log(err);
      this.setState({ loading : false })
      this.dbops.error("Error fetching data");
    })

  }

  getRealTimeUpdate = () =>{

    this.dbops.realtime_update(
      Config.NETWORK.submissions+'/'+this.props.assessment.key+'/comments/',
      (querySnapshot) => {
        // console.log("Update of comment ", snap);
        let t = [];

        querySnapshot.forEach(function(doc) {
          let ojbm = doc.data();

          ojbm.key = doc.id;
          t.push(ojbm);
        });
        // console.log("fetched comments", t);
        this.updateComments(t);
      }
    );
    // docRef.onSnapshot(function(doc){
    //     if(doc && doc.exists){
    //         const myQuote=doc.data();
    //         console.log("Check out this document I received ",doc);
    //         outputHeader.innerText="My Inspirational Quote: "+myQuote.inspirationalQuote;
    //     }
    // });
  }

  saveComment = ( message ) => {
    // save_data_fire_promise = (collection, doc, value)
    let subdata = {
      "aid" : this.props.assessment.aid,
      "supervisorid" : this.dbops.get_current_user(),
      "comment" : message,
    };
    //save_data_fire_push_promise = (collection,value, callback, error)
    this.dbops.save_data_fire_push_promise(
        Config.NETWORK.submissions+'/'+this.props.assessment.key+'/comments/',
        subdata
      ).then(()=>{
        this.dbops.success("Commented Successfully");
        this.setState({ lastSubmission : subdata, msgbox : ''});
      }).catch(()=>{
        this.dbops.success("Comment failed");
      })
  }


  render() {
    return (
      <div>

      <Breadcrumb.Item href="#">
        <span> &nbsp;&nbsp;{this.state.comments.length} Comment{this.state.comments.length>0?'s':''}</span>
      </Breadcrumb.Item>

      <Modal
          visible={this.props.show}
          width={'70%'}
          title={this.props.item}
          okText={'Comment'}
          onOk={()=>{
            this.saveComment(this.state.msgbox);
          }}
          onCancel={()=>{
            if(this.props.onCancel)
              this.props.onCancel();
          }}
        >

        <h3>Comments</h3>

        <div style={{ maxHeight: '300px', overflowY: 'scroll',}}>
        <List
          itemLayout="horizontal"
          dataSource={this.state.comments}
          renderItem={item => (
            <List.Item
            >
              <List.Item.Meta
                avatar={
                  item.supervisorid ? <Avatar src={'/static/images/boss.svg'} style={{ color: '#f56a00'}} /> : ''
                }
                title={<a href="https://ant.design">{item.comment}</a>}
                description={moment(item.time).format('lll')}

              />
              <div>
              { item.studentid ?
              <Avatar src={'/static/images/boy.svg'} style={{ color: '#f56a00'}} /> : ''
              }
              </div>
            </List.Item>
          )}
        />
        </div>

        <TextArea
        onChange={(e)=>{
          e.preventDefault();
          // console.log(e.target.value);
          this.setState({ msgbox : e.target.value })
        }}
        style={{ minHeight: '120px' }}
        placeholder="Enter your message to your supervisor" autosize={{ minRows: 6, maxRows: 12 }} value={this.state.msgbox} />

      </Modal>
      </div>
    );
  }
}

export default LoadMoreList;
