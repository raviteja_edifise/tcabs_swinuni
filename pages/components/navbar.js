import React from 'react';

import { redirect } from '../util/dbops';

// import { Menu, Icon, Badge } from 'antd';
import Menu from 'antd/lib/menu';
import Icon from 'antd/lib/icon';
import Badge from 'antd/lib/badge';
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

import style_menu from 'antd/lib/menu/style/index.css';
// import { MenuCSS } from "../../styles/styles.js";

import Config from '../util/config';

export default class App extends React.Component{
  state = {
    current: '',
  }
  constructor(props){
      super(props);
  }

  componentDidMount(){
  }

  // handleClick = (e) => {
  //   this.setState({
  //     current: e.key,
  //   });
  //   //redirect(e.key);
  // }

  render(){
      return (
          <div>

          {/* <style jsx global>{MenuCSS}</style> */}
          <style dangerouslySetInnerHTML={{ __html: style_menu }} />

            <Menu
              selectedKeys={[this.state.current]}
              mode="horizontal"
            >
              <Menu.Item key="activate" >
                  <a href="/"><Icon type="home" /></a>
              </Menu.Item>
               <Menu.Item key="activate" >
                   <a href="/adduser"><Icon type="solution" />Add User</a>
               </Menu.Item>

               {/*
              <Menu.Item key="activity" >
                  <Icon type="line-chart" />Activity
              </Menu.Item>
              <Menu.Item key="rereward" >
                  <Icon type="line-chart" />Re Reward
              </Menu.Item>
              */}

              <Menu.Item>
                <Badge dot>
                  <span style={{ color : Config.isDebug ? 'orange': 'red', fontWeight:'bold' }}>  {Config.isDebug ?'DEV MODE':'LIVE MODE'} </span>
                </Badge>
              </Menu.Item>

            </Menu>

          </div>
      );
  }
}
