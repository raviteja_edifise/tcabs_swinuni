import React from 'react';


import { Form, Input, Tooltip, Icon, Cascader, Select, Row, Col, Divider,
  Checkbox, Button, AutoComplete,message, Modal, DatePicker, Radio, Badge, Spin } from 'antd';
import moment from 'moment';
import is from 'is_js';

// import { Menu, Icon, Badge, Form, Button, message, Modal } from 'antd';
const FormItem = Form.Item;
const Option = Select.Option;
const AutoCompleteOption = AutoComplete.Option;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

import Config from '../util/config';
import CustomError from '../util/errors/errors';
import { validateEmail, PhoneNumberValidate, formatPhoneNumber, mergeObjs, removeKey } from '../util/util';
import dbops from '../util/dbops';

class AddUnit extends React.Component{
  state = {
    data: null,
    loading : false,
    dataSource: [],
    lastSearch: '',
    searchId : null
  }
  constructor(props){
      super(props);
      this.dbops = new dbops();
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const { validateFieldsAndScroll, validateFields, resetFields } = this.props.form;
    validateFields(['teamid'], { force : true },(err, values) => {
      if (!err) {
        // console.log('Received values of form: ', values);

        this.setState({ loading: true })
        var editObj = {};
        editObj.projectid = this.props.chosenProject.projectid;
        editObj.teamid = values.teamid;

        //save_data_fire_push = (collection,value, callback, error)
        // this.dbops.save_data_push(Config.NETWORK.adminAddEnrolment+'/', editObj,(resp)=>{
        //   this.dbops.success('Enroled successfully');
        //   this.setState({ loading : false });
        //   if(this.props.onSave)
        //       this.props.onSave();
        //   resetFields();
        //   // this.dbops.redirect(Config.url_admin);
        // }, (err) => {
        //   // debugger;
        //   console.log(err);
        //   this.dbops.error('Error in operation');
        //   this.setState({ loading : false });
        // })

        this.handleSearch(values.teamid, (r)=>{
          // alert(JSON.stringify(r));

          if(r.length > 0)
          {
            this.setState({ loading: false })
            this.dbops.error('Team ID already Exists');
            return;
          }

          this.dbops.save_data_fire(Config.NETWORK.convenorTeam, this.props.chosenProject.projectid+values.teamid,  editObj,(resp)=>{
            this.dbops.success('Team added successfully');
            this.setState({ loading : false });
            if(this.props.onSave)
                this.props.onSave();
            resetFields();
            // this.dbops.redirect(Config.url_admin);
          }, (err) => {
            // debugger;
            console.log(err);
            this.dbops.error('Error in operation');
            this.setState({ loading : false });
          })
        })
        //  Firestore



      }
    });
  }

  handleSearch = (value, callback) => {
    if(value.length <= 4)
    {
      this.setState({
        dataSource : []
      })
    }

    if(value.length > 4 && value != this.state.lastSearch){
      this.setState({ lastSearch : value, dataSource : []});


      //autocomplete_fire = (collection, queryKey, queryString, limit, callback, error)
      //this.dbops.search_fire(Config.NETWORK.convenorTeam, 'teamid', '==', value, 2, (querySnapshot)=>{
      this.dbops.queryFS(Config.NETWORK.convenorTeam,
        [{
          key : "teamid",
          comp : "==",
          val : value
        },{
          key : "projectid",
          comp : "==",
          val : this.props.chosenProject.projectid
        }], 2, null, null, (querySnapshot)=>{
        let t = [];
        querySnapshot.forEach(function(doc) {
          // console.log(doc.id, " => ", doc.data());
          t.push(doc.id);
        });

        callback(t);
        // this.setState({ dataSource : t});
        // console.log(child);
      }, (err)=>{
        // console.log(err);
        this.dbops.error("Error fetching data");
      })

      // this.dbops.autocomplete('/students/', value, (child)=>{
      //   let t = this.state.dataSource;
      //   t.push(child.id);
      //   this.setState({ dataSource : t});
      //   // console.log(child);
      // }, (err)=>{
      //   this.dbops.error("Error fetching data");
      // })


    }
  }


  onSelect = (value) => {
    let { setFieldsValue } = this.props.form;
    setFieldsValue({ searchId : value})
    // console.log('onSelect', value);
    // this.setState({ searchId : value})
  }

  render(){
    const { getFieldDecorator } = this.props.form;
    const { dataSource } = this.state

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 16,
          offset: 8,
        },
      },
    };

      return (
        <Row type="flex" justify="center" align="top">

        <Col span={24}>
          <Spin spinning={this.state.loading} >
          {/* <style jsx global>{MenuCSS}</style> */}

          <Form layout="inline" className={' marLR25'} onSubmit={this.handleSubmit}>
          <Divider />
          <FormItem
            {...formItemLayout}
            label={(
              <span>
                 Team ID&nbsp;
              </span>
            )}
          >
            {getFieldDecorator('teamid', {
              initialValue : this.state.teamid ? this.state.teamid : '',
              rules: [{ required: true, message: 'cant be empty!', whitespace: false },
              { validator : (rule, value, callback)=> {
                //const { getFieldValue } = this.props.form;
                let unitid = value;

                if( (''+unitid).length >3 && (''+unitid).length <= 8 )
                {
                  if(is.alphaNumeric(unitid) )
                    callback();
                  else
                    callback('Cannot have special characters');
                }
                callback(' Ex : TEAM001 && length 4 to 8 characters');
              }}
            ],
            })(

              <Input />
            )}
          </FormItem>

          <FormItem>
            <Button
              type="submit"
              htmlType="submit"
            >
              Add Team
            </Button>
          </FormItem>

          </Form>

        </Spin>

        <br/>
        <br/>
          </Col>
        </Row>
      );
  }
}

const AddUnitComp = Form.create()(AddUnit);
export default AddUnitComp;
