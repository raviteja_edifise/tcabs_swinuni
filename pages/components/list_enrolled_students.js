import React from 'react';
import { List, Avatar, Button, Spin, Modal, message, Badge, Col, Row } from 'antd';
const confirm = Modal.confirm
import reqwest from 'reqwest';
import moment from 'moment';

import EditStudent from './edit_employee';
import dbops from '../util/dbops';
import Config from '../util/config';
import { mergeObjs, getIndexOfAttrInObj } from '../util/util';

import AddEnrolment from './add_enrolment';

const fakeDataUrl = 'https://randomuser.me/api/?results=5&inc=name,gender,email,picture,phone';

class LoadMoreList extends React.Component {
  state = {
    loading: true,
    loadingMore: false,
    showLoadingMore: true,
    data: [],
    lastKey : null,
    pagingCount : 10,
    edit_visible : false,
    editData : null,
    disableUserId : null
  }

  constructor(){
    super();
    this.dbops = new dbops();
  }
  componentDidMount() {

    this.getStudentsData((res) => {

      let rs = res;
      this.setState({
        loading: false,
        data: rs,
      });
    });
  }

  editStudentOpen = (data) => {
    // debugger;
    if(this.refs.editStudent)
      this.refs.editStudent.editStudent(true);
  }

  getStudentsData = (callback) => {
    // debugger;
    const chosenUnit = this.props.chosenUnit;
    if(!chosenUnit){

      callback([])
      return;
    }
    this.setState({loading: true});
    var path = Config.NETWORK.adminAddEnrolment;
    this.dbops.queryFS(path, [
      {
        key : "unitid",
        comp : "==",
        val : chosenUnit.unitid
      }
    ], this.state.pagingCount, null, null, (resp)=>{
      this.setState({loading: false});
      let tempA = new Array();
      let firstTime = true;
      var templastKey = null;

      resp.forEach((doc) => {
        //console.log(doc.id, " => ", doc.data());
        let data = doc.data();
        data.key = doc.id;
        tempA.push(data);
        templastKey = tempA[tempA.length - 1].time
        firstTime = false;
      });
      this.setState({ lastKey : templastKey});
      if(tempA.length < this.state.pagingCount)
          this.setState({showLoadingMore : false});
      callback(tempA);
    }, (err)=>{
      console.log("errro ", err);
      this.setState({loading: false});
    })
      // var queryParams = {
      //       paging : this.state.pagingCount,
      //       lastKey : this.state.lastKey,
      //       orderBy : 'unitid',
      //       order: 'desc'
      //   };
      //
      //   var path = Config.NETWORK.adminAddEnrolment;
      //   this.dbops.query(path, queryParams, (resp)=>{
      //     // console.log(resp.val());
      //     // debugger;
      //     var dealsData = resp;
      //     let tempA = new Array();
      //
      //     if(!dealsData)
      //     {
      //       callback(tempA);
      //       return;
      //     }
      //
      //     dealsData.forEach((child) => {
      //
      //         let eachkey = child.key;
      //         if(eachkey === this.state.lastKey){}
      //         else {
      //           let data = child.val();
      //           data.key = eachkey;
      //           tempA.push(data);
      //         }
      //
      //     });
      //     tempA.reverse();
      //     var templastKey = null;
      //     if(tempA.length > 0)
      //       templastKey = tempA[tempA.length - 1].key;
      //
      //     // alert(templastKey);
      //     this.setState({ lastKey : templastKey});
      //     //
      //     // //alert(tempA.length);
      //     if(tempA.length < this.state.pagingCount)
      //         this.setState({showLoadingMore : false});
      //     callback(tempA);
      //
      //   }, (err)=> {
      //     message.error(Config.UI.database_error)
      //   })
  }


  onLoadMore = () => {

    this.setState({
      loadingMore: true,
    });
    this.getStudentsData((res) => {
      const data = this.state.data.concat(res);
      this.setState({
        data,
        loadingMore: false,
      }, () => {
        window.dispatchEvent(new Event('resize'));
      });
    });
  }

  disableUnit = (item, newstatus) => {
    // console.log(item.uid);
    confirm({
    title: 'Are you sure '+(newstatus == "active" ? 'enable' : 'disable')+' this unit?',
    content: '',
    okText: 'Yes',
    okType: 'danger',
    cancelText: 'No',
    onOk: () => {
      this.dbops.save_data(Config.NETWORK.adminAddUnit+'/'+item.unitid+'/status', newstatus,(resp)=>{
        this.dbops.success('Unit updated successfully');
        this.setState({ loading : false });
        this.refreshList();
      }, (err) => {
        this.dbops.error('Error in operation');
        this.setState({ loading : false });
      })
    },
    onCancel: () => {},
  });
  }

  refreshList = () => {

    this.setState({ data : [], lastKey : null})
    this.getStudentsData((res) => {
      let rs = res;
      this.setState({
        loading: false,
        data: rs,
      });
    });
  }


  render() {

    const { loading, loadingMore, showLoadingMore, data } = this.state;
    const chosenUnit = this.props.chosenUnit;


    const loadMore = showLoadingMore ? (
      <div style={{ textAlign: 'center', marginTop: 12, height: 50, lineHeight: '50px' }}>
        {loadingMore && <Spin />}
        {!loadingMore && <Button className={' '} onClick={this.onLoadMore}>loading more</Button>}
      </div>
    ) : null;
    return (
      <Row type="flex" justify="center" align="top">
      <Col span={24}>

      {chosenUnit == null &&
        <div className={' center'}>
          <Avatar src ={'/static/images/pointing-left.svg'} className={'wh100px'}/>
          <div>Choose Unit</div>
        </div>
      }

      {chosenUnit &&
        <div>
      <h3 className={'color1 '}>Enrolments for {chosenUnit.unitname}</h3>
      <h4>Unit ID : {chosenUnit.unitid}</h4>

      <Spin spinning={this.state.loading} >
      <AddEnrolment onSave={this.refreshList} chosenUnit={this.props.chosenUnit} />

      <List
        className="demo-loadmore-list"
        loading={loading}
        itemLayout="horizontal"
        loadMore={loadMore}
        dataSource={data}
        renderItem={item => (
          <List.Item actions={[

            <Button type="danger" onClick={
              ()=>{
                // // this.editStudentOpen(item);
                // let newstatus = "active";
                // if(item.status == "active")
                //   newstatus = "inactive";
                //

                // if(item.status == "inactive")
                //   newstatus = "active";
                //
                // this.disableUnit(item, newstatus);

              }} >{'Remove'}</Button>]}>
            <List.Item.Meta
              title={<a href="#">{item.studentid}</a>}
              description={moment(item.time).format('L')}
            />
          </List.Item>
        )}
      />
        </Spin>
        </div>
      }

      </Col>
      </Row>

    );
  }
}

export default LoadMoreList;
