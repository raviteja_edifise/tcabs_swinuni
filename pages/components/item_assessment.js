import React from 'react';
import { List, Avatar, Button, Progress, Spin, Modal, message, Badge, Col, Row, Card, Icon,  Upload } from 'antd';
const { Meta } = Card;
const confirm = Modal.confirm
import reqwest from 'reqwest';

import Iframe from 'react-iframe';
import moment from 'moment';

import EditStudent from './edit_employee';
import dbops from '../util/dbops';
import Config from '../util/config';
import { mergeObjs, getIndexOfAttrInObj, openInNewTab } from '../util/util';
import UploadComp from './upload';

import CommentsUI from './ui_comments_assessment';

class LoadMoreList extends React.Component {
  state = {
    loading: true,
    projinfo : null,
    chosenDescription : false,
    lastSubmission : false,
    tip: '',
    showComments : false
  }

  constructor(){
    super();
    this.dbops = new dbops();
  }
  componentDidMount() {
//submissions
    this.getsubmission(this.props.item.aid+"_"+this.dbops.get_current_user());
  }

  getsubmission = (docid) => {
    // debugger;
    this.setState({loading : true});
    this.dbops.get_data_fire(Config.NETWORK.submissions, docid).then(r => {
      // console.log("sssd ",r.data());
      let fetchedDoc = r.data();
      if(fetchedDoc)
        this.setState({ lastSubmission : fetchedDoc, loading : false})
      else {
        this.setState({loading : false});
      }
    })
  }

  savesubmission = ( filepath ) => {
    // save_data_fire_promise = (collection, doc, value)
    let subdata = {
      "aid" : this.props.item.aid,
      "studentid" : this.dbops.get_current_user(),
      "uploadURL" : filepath,
    };
    this.dbops.save_data_fire_promise(
        Config.NETWORK.submissions,
        this.props.item.aid+"_"+this.dbops.get_current_user(),
        subdata
      ).then(()=>{
        this.dbops.success("File Uploaded Successfully");
        this.setState({ lastSubmission : subdata});
      }).catch(()=>{
        this.dbops.success("Submission failed");
      })
  }

  render() {
    const { loading } = this.state;

    let currentDate = moment();
    let dueDate = this.props.item.due;

    return (
      <Col span={11} gutter={10} style={{ marginRight: '3%', marginBottom: '10px'}}

      >
      <Spin spinning={this.state.loading} tip={this.state.tip}>
        <Card title={this.props.item.aname} bordered={false}
        actions={[
          <Badge count={0} dot onClick={()=>{
            this.setState({ chosenDescription : this.props.item.adescription });
          }}>
            <Icon type="eye-o" /> &nbsp;View
          </Badge>,
          <Badge count={0} dot onClick={()=>{
            this.setState({ showComments : true });
          }}>
            <Icon type="message" /> &nbsp;Comments
          </Badge>
          ]}
        >
        ID : {this.props.item.aid}
        <br/><br/>
        DUE : <Badge count={moment(this.props.item.due).format('lll')} style={{ backgroundColor: '#52c41a' }} />
        <br/><br/>

        GRADE
        {this.state.lastSubmission.grade &&
          <Progress percent={this.state.lastSubmission.grade} />
        }
        <br/><br/>

        {!this.state.lastSubmission && currentDate.diff(dueDate, 'minutes') < 0 &&
        <UploadComp
        icon="upload" message="upload " filetype="application/pdf" fileext="pdf" maxSize="5000000"
        onFile={(file)=>{
          // console.log(file);
          // (file, filepath, progressf, successf, errorf)
          this.setState({loading : true});
          this.dbops.uploadFile(file.originFileObj,
            Config.NETWORK.submissions+'/'+this.props.item.aid+"_"+this.dbops.get_current_user()+".pdf",
            (per)=>{
              // console.log(per)
              this.setState({tip : Math.floor(per)+"% completed"});
            },
            (r)=>{
              //debugger;
              this.setState({loading : false});
              // console.log('done', r);
              this.setState({tip : "Processing..."});
              this.savesubmission(r);

            }, (e)=>{
              this.setState({loading : false});
              console.log('erro ', e)
              this.dbops.success("Submission failed");
          })
        }}/>
        }

        {this.state.lastSubmission &&
          <div>

          <Button type="primary" loading={loading} onClick={()=>{
            openInNewTab(this.state.lastSubmission.uploadURL)
          }}
          style={{ backgroundColor : '#8E44AD', borderColor : '#8E44AD'}}
          >
            View Submission &nbsp;&nbsp;
            <Badge
              style={{ backgroundColor: '#fff', color: '#999' }}
              count={moment(this.state.lastSubmission.time).format('lll')}
              />
          </Button>
          <br/><br/>
          {/* cannot submit after due date */}
          {currentDate.diff(dueDate, 'minutes') <0 &&
          <Button type="dashed" icon="reload" loading={loading} onClick={()=>{
            this.setState({ lastSubmission : false });
          }}>
            Resubmit
          </Button>
          }

          </div>

        }


        <CommentsUI
          show={this.state.showComments}
          onCancel={()=>{ this.setState({ showComments : false})}}
          assessment={this.props.item}
        />

        </Card>



        <Modal
            visible={!!this.state.chosenDescription}
            width={'70%'}
            title={this.props.item.aid}
            onOk={()=>{
              this.setState({ chosenDescription: false})
            }}
            onCancel={()=>{
              this.setState({ chosenDescription: false})
            }}
            footer={[
              <Button key="submit" type="primary" loading={loading} onClick={()=>{
                this.setState({ chosenDescription: false})
              }}>
                Close
              </Button>,
            ]}
          >

            { this.state.chosenDescription &&
            <Iframe url={this.state.chosenDescription}
            width={'100%'}
            height="450px"
            id="myId"
            className="myClassname"
            display="initial"
            position="relative"
            allowFullScreen/>
            }

        </Modal>
        </Spin>
      </Col>
    );
  }
}

export default LoadMoreList;
