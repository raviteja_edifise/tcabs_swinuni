import React from 'react';
import { List, Avatar, Button, Spin, Modal, message } from 'antd';
const confirm = Modal.confirm
import reqwest from 'reqwest';

import EditStudent from './edit_student';
import dbops from '../util/dbops';
import Config from '../util/config';
import { mergeObjs, getIndexOfAttrInObj } from '../util/util';

const fakeDataUrl = 'https://randomuser.me/api/?results=5&inc=name,gender,email,picture,phone';

class LoadMoreList extends React.Component {
  state = {
    loading: true,
    loadingMore: false,
    showLoadingMore: true,
    data: [],
    lastKey : null,
    pagingCount : 25,
    edit_visible : false,
    editData : null,
    disableUserId : null
  }

  constructor(){
    super();
    this.dbops = new dbops();
  }
  componentDidMount() {

    this.getStudentsData((res) => {

      let rs = res;
      //Remove the first duplicate element
      // if(this.state.data.length == 0){
      //   let popped = rs.pop();
      // }

      this.setState({
        loading: false,
        data: rs,
      });
    });
  }

  editStudentOpen = (data) => {
    debugger;
    if(this.refs.editStudent)
      this.refs.editStudent.editStudent(true);
  }


  getData = (callback) => {
    reqwest({
      url: fakeDataUrl,
      type: 'json',
      method: 'get',
      contentType: 'application/json',
      success: (res) => {
        callback(res);
      },
    });
  }

  getStudentsData = (callback) => {
    // debugger;

      this.dbops.queryFS('students', [], this.state.pagingCount, this.state.lastKey, 'time', (resp)=>{
        let tempA = new Array();
        let firstTime = true;
        var templastKey = null;

        resp.forEach((doc) => {
          //console.log(doc.id, " => ", doc.data());
          let data = doc.data();
          data.key = doc.id;
          tempA.push(data);
          templastKey = tempA[tempA.length - 1].time
          firstTime = false;
        });
        this.setState({ lastKey : templastKey});
        if(tempA.length < this.state.pagingCount)
            this.setState({showLoadingMore : false});
        callback(tempA);
      }, (err)=>{
        console.log("errro ", err);
      })
      /*
      var queryParams = {
            paging : this.state.pagingCount,
            lastKey : this.state.lastKey,
            orderBy : 'key',
            order: 'desc'
        };

        var path = '/students/';

        this.dbops.query(path, queryParams, (resp)=>{

          // debugger;
          var dealsData = resp;
          let tempA = new Array();

          if(!dealsData)
          {
            callback(tempA);
            return;
          }

          dealsData.forEach((child) => {
              let eachkey = child.key;
              if(eachkey === this.state.lastKey){}
              else {
                let data = child.val();
                data.key = eachkey;
                tempA.push(data);
              }

          });
          tempA.reverse();
          var templastKey = null;
          if(tempA.length > 0)
            templastKey = tempA[tempA.length - 1].key;

          // alert(templastKey);
          this.setState({ lastKey : templastKey});
          //
          // //alert(tempA.length);
          if(tempA.length ==0)
              this.setState({showLoadingMore : false});
          callback(tempA);

        })
      */

  }


  onLoadMore = () => {

    this.setState({
      loadingMore: true,
    });
    this.getStudentsData((res) => {
      const data = this.state.data.concat(res);
      this.setState({
        data,
        loadingMore: false,
      }, () => {
        window.dispatchEvent(new Event('resize'));
      });
    });
  }

  disableUser = (item, newstatus) => {
    // console.log(item.uid);
    confirm({
    title: 'Are you sure '+(newstatus == "enabled" ? 'enable' : 'disable')+' this user?',
    content: '',
    okText: 'Yes',
    okType: 'danger',
    cancelText: 'No',
    onOk: () => {
      // console.log('OK');
      console.log(item.uid, newstatus);
      this.dbops.disableUser(Config.NETWORK.adminDisableUser, item.uid, newstatus, (res)=>{
        message.success((newstatus == "enabled" ? 'enable' : 'disable')+' '+Config.UI.user_disable_success)
        setTimeout(()=> {
          this.dbops.redirect(Config.url_admin_student.routerURL);
        }, 500);
      }, (e)=>{
        message.error((newstatus == "enabled" ? 'enable' : 'disable')+' '+Config.UI.user_disable_error)
      })
    },
    onCancel: () => {
      // console.log('Cancel');
    },
  });
  }


  render() {
    const { loading, loadingMore, showLoadingMore, data } = this.state;
    const loadMore = showLoadingMore ? (
      <div style={{ textAlign: 'center', marginTop: 12, height: 50, lineHeight: '50px' }}>
        {loadingMore && <Spin />}
        {!loadingMore && <Button className={' '} onClick={this.onLoadMore}>loading more</Button>}
      </div>
    ) : null;
    return (
      <div>

      <EditStudent
        ref="editStudent"
        studentData={this.state.editData}
        onSave={(newData)=>{
          this.dbops.success('Updated Successfully');

          // debugger;

          let currentId = getIndexOfAttrInObj(this.state.data, 'uid', newData.uid)
          console.log(currentId);
          if(currentId >= 0)
          {
            let modObj = this.state.data;
            modObj[currentId] = newData;
            this.setState({editData : null, data: modObj});
          }
          else {
            this.setState({editData : null });
          }
          // let modifiedObj = {};
          // modifiedObj[newData.uid] = newData;
          // let newStateObj = mergeObjs( this.state.data, modifiedObj);
          // this.setState({editData : null, data: newStateObj});
        }}
        onCancel={()=>{
          this.setState({editData : null});
        }}
        />

      <List
        className="demo-loadmore-list"
        loading={loading}
        itemLayout="horizontal"
        loadMore={loadMore}
        dataSource={data}
        renderItem={item => (
          <List.Item actions={[<Button icon="edit" onClick={
            ()=>{
              // this.editStudentOpen(item);
              this.setState({ editData : item })
            }} >EDIT</Button>,
            <Button type="danger" icon="edit" onClick={
              ()=>{
                // this.editStudentOpen(item);
                let newstatus = "enabled";
                if(item.status == "enabled")
                  newstatus = "disabled";

                if(item.status == "disabled")
                  newstatus = "enabled";

                this.disableUser(item, newstatus);
              }} >{item.status == "enabled" ? "disable" : (item.status == "disabled" ? "enable" : "")}</Button>]}>
            <List.Item.Meta
              avatar={<Avatar large="true" src={item.picture?item.picture:''} />}
              title={<a href="#">{item.firstname+' '+item.lastname}</a>}
              description={item.email}
            />
            <div>{item.phone}</div>
          </List.Item>
        )}
      />

      </div>
    );
  }
}

export default LoadMoreList;
