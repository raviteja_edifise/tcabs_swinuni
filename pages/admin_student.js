import Head from 'next/head';
import Link from 'next/link';
import React, { Component } from 'react';
import {Row , Col, Upload, Form, Button, Icon, Tabs, List, Avatar, Spin, Divider } from 'antd';
const FormItem = Form.Item;
const TabPane = Tabs.TabPane;
const ButtonGroup = Button.Group;
const antIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;

import Papa from 'papaparse';

//styles for components
// import { DatepickerCSS, MenuCSS } from "../styles/styles.js";
import NavBarAdmin from './components/navbar_admin';
import UploadComp from './components/upload';
import ListStudents from './components/list_students';
import AddStudent from './components/add_student';
import dbops from './util/dbops';
import Config from './util/config';
import { PhoneNumberValidate, formatPhoneNumber} from './util/util';
/**
 * this is AdminStudent.
 */
class AdminStudent extends Component{
  /**
   * @param {number} a - this is a value.
   * @param {number} b - this is a value.
   * @return {number} result of the sum value.
   */
  state = {
    usersData : [],
    usersSingleData : [],
    errorsData : [],
    loading : false,
    addStudent : false
  }

  constructor(){
    super();
    this.dbops = new dbops();
  }

  componentDidMount(){

    this.dbops.listen_user_events( (user)=>{

      this.dbops.get_user_type((data)=>{
        console.log(data);
        this.dbops.log(data.usertype !== data);
        switch(data.usertype){
          case "admin": break;
          case "student": this.dbops.redirect(Config.url_student);break;
          case "convenor": this.dbops.redirect(Config.url_convenor);break;
          case "supervisor": this.dbops.redirect(Config.url_supervisor);break;
          default :
        }
      })
    }, (err)=>{
      this.dbops.error('Sign In Error');
    })
  }

  tabChanged = (key) => {
    // console.log(key);
  }

  onFileChoosen = (value) => {
    // console.log(value);
    this.setState({ errorsData : []});

    Papa.parse(value.originFileObj, {
    	complete: (results) => {
    		//console.log("Finished:", results.data);
        let uData = [], usingleData=[], eData = [];

          // debugger;
        for(var i=0; i< results.data.length-1; i++)
        {
          let eachUData = results.data[i];
          let phoneNum = eachUData[6];

          if(PhoneNumberValidate(phoneNum))
          {
            uData.push({
              uid : eachUData[0],
              displayName: eachUData[2],
              email: eachUData[0]+'@student.swin.edu.au',
              photoURL: eachUData[5],
              phoneNumber: formatPhoneNumber(phoneNum),
              customClaims: { usertype : "student"},
            });

            usingleData.push({
              uid : eachUData[0],
              firstname : eachUData[1],
              lastname : eachUData[2],
              email : eachUData[3],
              gender : eachUData[4],
              picture : eachUData[5],
              contact : eachUData[6],
              address : eachUData[7],
              status : "enabled",
              dob : (new Date(eachUData[8])).getTime(),
              time : (new Date()).getTime()
            })
          }
          else {
            eData.push({
              uid : eachUData[0],
              displayName: eachUData[1],
              email: eachUData[2],
              phoneNumber: eachUData[3]
            })
          }
        }//end of for loop
            // console.log(uData);
        this.setState({ usersData : uData, usersSingleData : usingleData, errorsData : eData});
    	}
    });

  }

  uploadStudents = () => {
    //idToken
    // alert()
    this.setState({ loading : true });
    this.dbops.uploadStudents(Config.NETWORK.adminUsersImport, this.state.usersData, this.state.usersSingleData, (resp)=>{
      console.log(resp);
      this.dbops.success('Successfully Done');
      this.setState({ loading : false });

      this.dbops.redirect(Config.url_admin);
    }, (err) => {
      console.log(err);
      this.dbops.error('Error in operation');
      this.setState({ loading : false });
    })
  }

  render(){
    return <div className="header">
      <Head>
        <title>TON ADMIN</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />

      </Head>

      <NavBarAdmin pathname={this.props.url.pathname?this.props.url.pathname:null} />

      <Row type="flex" justify="center" align="top">
      <Col sm={22} md={21} lg={21} >
      <Tabs defaultActiveKey="1" onChange={this.tabChanged}>
        <TabPane tab={<span><Icon type="appstore-o" />Manage</span>} key="1">
          <ListStudents />
        </TabPane>
        <TabPane tab={<span><Icon type="plus-circle" />Add</span>} key="2">

          <Spin indicator={antIcon} spinning={this.state.loading} >
          <div>

          <Row type="flex" justify="center">
            <Col span={4}>

            <AddStudent
              studentData={this.state.addStudent}
              onSave={(newData)=>{
                this.dbops.success('Updated Successfully');

                let modObj = this.state.data;
                modObj.push(newData);
                this.setState({addStudent : false, data: modObj});
              }}
              onCancel={()=>{
                this.setState({addStudent : false});
              }}
            />

            <Button type="primary" size={"large"} icon="user-add"
            className={'center mar25 '}
            onClick={()=>{
              this.setState({ addStudent : true})
            }}
            >{Config.UI.student_add}</Button>

            </Col>
          </Row>

          <Divider>Or upload students</Divider>

          {this.state.usersData.length == 0 &&
          <UploadComp onFile={this.onFileChoosen} />
          }

          {this.state.usersData.length > 0 &&
            <List
              size="small"
              header={<div className={'marTB30 color1'}>
                <b>{this.state.usersData.length} students fetched from file</b>
                <Button type="primary" className={'right'} onClick={this.uploadStudents.bind(this)}>Click to Upload </Button>

                </div>}
              footer={<div></div>}
              bordered
              dataSource={this.state.usersData}
              renderItem={item => (
                <List.Item>
                  <List.Item.Meta
                    avatar={<Avatar src={item.photoURL} />}
                    title={<a href="#">{item.displayName}</a>}
                    description={item.email}
                  />
                  <div>{item.phoneNumber}</div>
                </List.Item>)}
            />
          }
          </div>
          </Spin>

        </TabPane>
      </Tabs>
      </Col>
      </Row>

      <Row type="flex" justify="center" align="top">
      <Col sm={22} md={18} lg={16} >




      </Col>
      </Row>


    </div>

  }
}


// const AdminStudentManage = Form.create()(AdminStudent);
export default AdminStudent;
