import Head from 'next/head';
import Link from 'next/link';
import React, { Component } from 'react';
import {Row , Col, Menu, Icon, Radio } from 'antd';
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;
import { AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip } from 'recharts';

import dbops from './util/dbops';
import Config from './util/config';
//styles for components
// import { DatepickerCSS, MenuCSS } from "../styles/styles.js";
import NavBarStudent from './components/navbar_student';
import UnitAssessments from './components/list_student_assessments';
/**
 * this is IndexClass.
 */
 let distinction = 70;
let max = 130;
 const data = [
      {name: 'Feb 10', uv: 60, pv: distinction, amt: max},
      {name: 'Feb 20', uv: 50, pv: distinction, amt: max},
      {name: 'Mar 10', uv: 70, pv: distinction, amt: max},
      {name: 'Mar 20', uv: 77, pv: distinction, amt: max},
      {name: 'Apr 10', uv: 58, pv: distinction, amt: max},
      {name: 'Apr 20', uv: 73, pv: distinction, amt: max},
      {name: 'May 10', uv: 34, pv: distinction, amt: max},
      {name: 'May 20', uv: 54, pv: distinction, amt: max},
      {name: 'June 10', uv: 64, pv: distinction, amt: max},
];

export default class extends Component{

  state = {
    loading : false,
    user : false,
    pagingCount : 25,
    unitsEnrolled : [],
    chosenUnit : null,
    distinction : 70,
    data : [
         {name: 'Feb 10', uv: 60, pv: distinction, amt: max},
         {name: 'Feb 20', uv: 50, pv: distinction, amt: max},
         {name: 'Mar 10', uv: 70, pv: distinction, amt: max},
         {name: 'Mar 20', uv: 77, pv: distinction, amt: max},
         {name: 'Apr 10', uv: 58, pv: distinction, amt: max},
         {name: 'Apr 20', uv: 73, pv: distinction, amt: max},
         {name: 'May 10', uv: 34, pv: distinction, amt: max},
         {name: 'May 20', uv: 54, pv: distinction, amt: max},
         {name: 'June 10', uv: 64, pv: distinction, amt: max},
   ]
  }

  constructor(){
    super();
    this.dbops = new dbops();
  }

  componentDidMount(){

    this.dbops.listen_user_events( (user)=>{
      this.setState({ user : user})
      this.dbops.get_user_type((data)=>{
        //console.log(data);

        this.dbops.log(data.usertype !== data);
        switch(data.usertype){
          case "admin": this.dbops.redirect(Config.url_admin);break;
          case "student": break;
          case "convenor": this.dbops.redirect(Config.url_convenor);break;
          case "supervisor": this.dbops.redirect(Config.url_supervisor);break;
          default :
        }
      });


      this.getUnitsofStudent((res) => {

        let rs = res;
        console.log('ddd',rs);
        this.setState({
          loading: false,
          unitsEnrolled: rs,
        });
      });


    }, (err)=>{
      this.dbops.error('Sign In Error');
    })
  }


  getUnitsofStudent = ( callback) => {
    // debugger;
    console.log(this.dbops.get_current_user())
    this.dbops.queryFS(Config.NETWORK.adminAddEnrolmentFire,
      [{
        key : "studentid",
        comp : "==",
        val : this.dbops.get_current_user()
      }], this.state.pagingCount, null, null, (querySnapshot)=>{
      let t = [];

      querySnapshot.forEach(function(doc) {
        let ojbm = doc.data();

        ojbm.key = doc.id;
        t.push(ojbm);
      });
      console.log("fetched records", t);
      callback(t);
    }, (err)=>{
      console.log(err);
      this.dbops.error("Error fetching data");
    })

  }


  handleClick = (e) => {
    // console.log('click ', e);
    this.setState({
      chosenUnit: null,
    }, ()=>{
      this.setState({ chosenUnit : e.key })
    });
  }

  handleSizeChange = (e) => {
      this.setState({ distinction: e.target.value });

      this.setState({ data : [
           {name: 'Feb 10', uv: 60, pv: e.target.value, amt: max},
           {name: 'Feb 20', uv: 50, pv: e.target.value, amt: max},
           {name: 'Mar 10', uv: 70, pv: e.target.value, amt: max},
           {name: 'Mar 20', uv: 77, pv: e.target.value, amt: max},
           {name: 'Apr 10', uv: 58, pv: e.target.value, amt: max},
           {name: 'Apr 20', uv: 73, pv: e.target.value, amt: max},
           {name: 'May 10', uv: 34, pv: e.target.value, amt: max},
           {name: 'May 20', uv: 54, pv: e.target.value, amt: max},
           {name: 'June 10', uv: 64, pv: e.target.value, amt: max},
     ]})
    }


  /**
   * @param {number} a - this is a value.
   * @param {number} b - this is a value.
   * @return {number} result of the sum value.
   */
  render(){
    return <div className="header">
      <Head>
        <title>TON STUDENT</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>

      <NavBarStudent />

      <Row type="flex" justify="center" align="top">
      <Col span={22}>

      {/* LEFT MENU */}
      <Col span={5}>

      <h3> Choose Unit </h3>
      <Menu
        onClick={this.handleClick}
        selectedKeys={[this.state.current]}
        mode="vertical"
      >
        {this.state.unitsEnrolled.map((item,index) => {
          return <Menu.Item key={item.unitid}>
            <Icon type="right" />{item.unitid}
          </Menu.Item>
        })}
      </Menu>

      </Col>

      {/* RIGHT MENU */}
      <Col span={19}>

      {this.state.chosenUnit &&
        <UnitAssessments unitid={this.state.chosenUnit} />
      }

      <Radio.Group value={this.state.distinction} onChange={this.handleSizeChange}>
        <Radio.Button value={80}>HD</Radio.Button>
        <Radio.Button value={70}>D</Radio.Button>
        <Radio.Button value={60}>C</Radio.Button>
        <Radio.Button value={50}>P</Radio.Button>
      </Radio.Group>

      {this.state.distinction}

      <AreaChart width={730} height={250} data={this.state.data}
        margin={{ top: 10, right: 30, left: 0, bottom: 0 }}>
        <defs>
          <linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
            <stop offset="5%" stopColor="#8884d8" stopOpacity={0.8}/>
            <stop offset="95%" stopColor="#8884d8" stopOpacity={0}/>
          </linearGradient>
          <linearGradient id="colorPv" x1="0" y1="0" x2="0" y2="1">
            <stop offset="5%" stopColor="#82ca9d" stopOpacity={0.8}/>
            <stop offset="95%" stopColor="#82ca9d" stopOpacity={0}/>
          </linearGradient>
        </defs>
        <XAxis dataKey="name" />
        <YAxis />
        <CartesianGrid strokeDasharray="3 3" />
        <Tooltip />
        <Area type="monotone" dataKey="uv" stroke="#8884d8" fillOpacity={1} fill="url(#colorUv)" />
        <Area type="monotone" dataKey="pv" stroke="#82ca9d" fillOpacity={1} fill="url(#colorPv)" />
      </AreaChart>


      </Col>
      </Col>
      </Row>

    </div>

  }
}
