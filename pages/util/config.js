/**
 * Created by Ravi on 23/9/17.
 */
const IMG_PATH = "/static/images/";
const API_URL = "https://us-central1-tcabs-swin.cloudfunctions.net"
//8E44AD
const networkURLS = {
  //STUDENTS API
  "adminUsersImport" : API_URL+"/adminUsersImport",
  "adminDisableUser" : API_URL+"/disableUser",
  "adminAddUser"     : API_URL+"/addUser",

  //EMPLOYEES API
  "adminEmployeesImport" : API_URL+"/adminEmployeesImport",
  "adminDisableEmployee" : API_URL+"/disableEmployee",
  "adminAddEmployee"     : API_URL+"/addEmployee",

  //UNITS API
  "adminAddUnit" : "/units",
  "adminAddUnitFire" : "units",
  "adminAddEnrolment" : "/enrolments",
  "adminAddEnrolmentFire" : "enrolments",

  //PROJECTS
  "convenorAddProject" : "projects",
  "convenorTeam" : "teams",
  "convenorAssessment" : "assessments",

  //TEAMS
  "convenorAddTeamMember" : "teamMembers",
  "convenorSupervisor" : "teamSupervisors",
  "submissions" : "submissions",
  "teamActivity" : "teamActivity",

  //Get UserInfo
  "userinfo" : API_URL+"/getCustomClaims"

};

const UI = {
  "user_save_success" : 'Save Successful',
  "user_save_error" : 'Error saving user profile',
  "user_disable_success" : ' Successful',
  "user_disable_error" : ' user ',
  "user_edit_success" : "",
  "student_add" : "Add Student",
  "employee_add" : "Add Employee",

  //Database
  "database_error" : 'Unable to fetch data. Please contact database administrator',
  "hillclimb" : IMG_PATH+"climbing.svg"
}

export default {
    "isDebug" : true,

    "UI" : UI,
    "TYPES" : {
      "ADMIN" : "ADMIN",
      "STUDENT" : "STUDENT",
      "CONVENOR" : "CONVENOR",
      "SUPERVISOR" : "SUPERVISOR"
    },

    "NETWORK" : networkURLS,

    "url_home" : "/", //home page
    "url_forgot" : "/forgot", //forgot

    //***** ADMIN PAGES ******
    "url_admin" : "/admin/student",  //Admin default page

    //***** ADMIN EMPLOYEE PAGE ******
    "url_admin_employee" : {
      "name" : "EMPLOYEES",
      "routerURL" : "/admin/employee", // this is server registered url name
      "fileName" : "admin_employee",   //this if filename
      "icon_active" : IMG_PATH+"user-icon.svg",
      "icon_normal" : IMG_PATH+"user-icon-dark.svg",
    },

    //***** ADMIN STUDENT PAGE ******
    "url_admin_student" : {
      "name" : "STUDENTS MANAGE",
      "routerURL" : "/admin/student", // this is server registered url name
      "fileName" : "admin_student",   //this if filename
      "icon_active" : IMG_PATH+"students-cap.svg",
      "icon_normal" : IMG_PATH+"students-cap-dark.svg",
    },

    //***** UNIT PAGES ******
    "url_admin_unit" : {
      "name" : "UNITS",
      "routerURL" : "/admin/unit", // this is server registered url name
      "fileName" : "admin_unit",   //this if filename
      "icon_active" : IMG_PATH+"open-book.svg",
      "icon_normal" : IMG_PATH+"open-book-dark.svg",
    },

    //***** ENROL STUDENT PAGES ******
    "url_admin_enrol" : {
      "name" : "ENROL",
      "routerURL" : "/admin/enrol", // this is server registered url name
      "fileName" : "admin_enrol",   //this if filename
      "icon_active" : IMG_PATH+"enrol.svg",
      "icon_normal" : IMG_PATH+"enrol-dark.svg",
    },

    "url_admin_logout" : {
      "name" : "LOGOFF",
      "routerURL" : "/", // this is server registered url name
      "fileName" : "admin_unit",   //this if filename
      "icon_normal" : IMG_PATH+"emergency-exit.svg",
    },

    // "url_admin_employee" : {
    //   "name" : "EMPLOYEES",
    //   "routerURL" : "/admin/employee",
    //   "fileName" : "admin_employee"
    // },

    // "url_admin_employee" : {
    //   "name" : "EMPLOYEES",
    //   "routerURL" : "/admin/employee",
    //   "fileName" : "admin_employee"
    // },

    "url_student" : "/student/dashboard",

    "url_student_dashboard" : {
      "name" : "  DASHBOARD",
      "routerURL" : "/student/dashboard", // this is server registered url name
      "fileName" : "student_dashboard",   //this if filename
      "icon_active" : IMG_PATH+"timeline.svg",
      "icon_normal" : IMG_PATH+"timeline-dark.svg",
    },

    "url_student_project" : {
      "name" : "  PROJECT",
      "routerURL" : "/student/project", // this is server registered url name
      "fileName" : "student_project",   //this if filename
      "icon_active" : IMG_PATH+"projects.svg",
      "icon_normal" : IMG_PATH+"projects-dark.svg",
    },

    "url_student_assessment" : {
      "name" : "  ASSESSMENTS",
      "routerURL" : "/student/assessment", // this is server registered url name
      "fileName" : "student_assessment",   //this if filename
      "icon_active" : IMG_PATH+"task-complete.svg",
      "icon_normal" : IMG_PATH+"task-complete-dark.svg",
    },

    //***** CONVENOR PAGES ******
    "url_convenor" : "/convenor/project",

    "url_convenor_project" : {
      "name" : "  PROJECTS",
      "routerURL" : "/convenor/project", // this is server registered url name
      "fileName" : "admin_project",   //this if filename
      "icon_active" : IMG_PATH+"projects.svg",
      "icon_normal" : IMG_PATH+"projects-dark.svg",
    },
    "url_convenor_team" : {
      "name" : "  TEAMS",
      "routerURL" : "/convenor/team", // this is server registered url name
      "fileName" : "convenor_team",   //this if filename
      "icon_active" : IMG_PATH+"team.svg",
      "icon_normal" : IMG_PATH+"team-dark.svg",
    },
    "url_convenor_assessment" : {
      "name" : "  ASSESSMENT",
      "routerURL" : "/convenor/assessment", // this is server registered url name
      "fileName" : "convenor_assessment",   //this if filename
      "icon_active" : IMG_PATH+"task-complete.svg",
      "icon_normal" : IMG_PATH+"task-complete-dark.svg",
    },


    //***** SUPERVISOR PAGES ******
    "url_supervisor" : "/supervisor/assessment",
    "url_supervisor_project" : {
      "name" : "  PROJECT",
      "routerURL" : "/supervisor/project", // this is server registered url name
      "fileName" : "supervisor_project",   //this if filename
      "icon_active" : IMG_PATH+"projects.svg",
      "icon_normal" : IMG_PATH+"projects-dark.svg",
    },

    "url_supervisor_assessment" : {
      "name" : "  ASSESSMENTS",
      "routerURL" : "/supervisor/assessment", // this is server registered url name
      "fileName" : "supervisor_assessment",   //this if filename
      "icon_active" : IMG_PATH+"task-complete.svg",
      "icon_normal" : IMG_PATH+"task-complete-dark.svg",
    },
}
