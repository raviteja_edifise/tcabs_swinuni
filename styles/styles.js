import css from 'styled-jsx/css';
// import 'antd/dist/antd.css';
class HandleCSS {
  replacestring = (mystring, index, replacement) => {
    return mystring.substr(0, index) + replacement+ mystring.substr(index + replacement.length);
  }

  handlecss = (mycss) => {
    let test1 = this.replacestring(mycss, 0, ' ');
    let testdating2 = this.replacestring(test1, test1.indexOf('*# sourceMappingUR')-3, ' ')
    return testdating2;
  }

  handlecss2 = (mycss) => {
    console.log(mycss);
    let test = css`${mycss}.root{color : blue}`;
    // debugger;
    console.log(test);
    return test;
  }
}

let csshandler = new HandleCSS();

//ABOUT PAGE CSS
import AboutCSS from './AboutCSS';
export const about = AboutCSS;

import Datepicker from '../node_modules/antd/lib/date-picker/style/index.css';
let DatepickerCSS = csshandler.handlecss2(Datepicker);
export {DatepickerCSS};

import Menu from 'antd/lib/menu/style/index.css';
let MenuCSS = css`${Menu}.root{ display: block}`;
export {MenuCSS};


let dating = css`.header{color : red}`;
export {dating};

/*
  ----- Global CSS -------
*/
export default css`
.body{}
`
